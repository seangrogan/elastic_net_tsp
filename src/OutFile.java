import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;

public class OutFile {
    private String filePath;
    private boolean append_to_file = true;
    private FileWriter write;
    private PrintWriter printWriter;

    public OutFile(String filePath, boolean append_to_file){
        this.filePath = filePath;
        this.append_to_file = append_to_file;
        File file = new File(filePath);
        try {
            this.write = new FileWriter(file, append_to_file);
        }
        catch (Exception e){
            System.out.println("Illegal file name or location: " + this.filePath);
        }
        this.printWriter = new PrintWriter(this.write);
    }
    public void writeLine(String line){
        this.printWriter.print(line);
        this.printWriter.print("\n");
    }
    public void closeFile(){
        this.printWriter.close();
    }

}
