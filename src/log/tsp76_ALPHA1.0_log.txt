+----------------------------+
| Sean Grogan's Elastic Net  |
+----------------------------+
Beginning: 2015-04-21 13:51:06

Paramters:
inFile     ./src/data/tsp76.txt
logFile    ./src/log/tsp76_ALPHA1.0_log.txt
outFile    ./src/out/tsp76_ALPHA1.0_out.csv
alpha      1.0
beta       0.2
initK      0.2
epsilon    0.02
KAlpha     0.99
KUpdate    25.0
NeuronsFac 2.5
radius     0.5
MaxIter    10000
DispIter   100

  0: 
	 Current Distance : 2.65385
	 Worst Distance   : 0.39766
	 Current Time     : 0.38 sec

  100: 
	 Current Distance : 1.77543
	 Worst Distance   : 0.35124
	 Current Time     : 24.43 sec

  200: 
	 Current Distance : 1.83964
	 Worst Distance   : 0.34705
	 Current Time     : 47.84 sec

  300: 
	 Current Distance : 1.96295
	 Worst Distance   : 0.33619
	 Current Time     : 70.27 sec

  400: 
	 Current Distance : 2.14897
	 Worst Distance   : 0.32881
	 Current Time     : 93.7 sec

  500: 
	 Current Distance : 2.38214
	 Worst Distance   : 0.31737
	 Current Time     : 116.33 sec

  600: 
	 Current Distance : 2.5705
	 Worst Distance   : 0.29921
	 Current Time     : 139.28 sec

  700: 
	 Current Distance : 2.83352
	 Worst Distance   : 0.28058
	 Current Time     : 162.29 sec

  800: 
	 Current Distance : 3.16329
	 Worst Distance   : 0.26897
	 Current Time     : 185.95 sec

  900: 
	 Current Distance : 3.50283
	 Worst Distance   : 0.25491
	 Current Time     : 209.14 sec

  1000: 
	 Current Distance : 3.8412
	 Worst Distance   : 0.24589
	 Current Time     : 232.23 sec

  1100: 
	 Current Distance : 4.1462
	 Worst Distance   : 0.23608
	 Current Time     : 254.96 sec

  1200: 
	 Current Distance : 4.36528
	 Worst Distance   : 0.22645
	 Current Time     : 277.73 sec

  1300: 
	 Current Distance : 4.59716
	 Worst Distance   : 0.21601
	 Current Time     : 300.8 sec

  1400: 
	 Current Distance : 4.86559
	 Worst Distance   : 0.21
	 Current Time     : 323.57 sec

  1500: 
	 Current Distance : 5.14097
	 Worst Distance   : 0.19812
	 Current Time     : 345.99 sec

  1600: 
	 Current Distance : 5.55965
	 Worst Distance   : 0.17544
	 Current Time     : 368.7 sec

  1700: 
	 Current Distance : 6.17959
	 Worst Distance   : 0.16911
	 Current Time     : 393.42 sec

  1800: 
	 Current Distance : 6.34607
	 Worst Distance   : 0.13436
	 Current Time     : 417.51 sec

  1900: 
	 Current Distance : 6.48908
	 Worst Distance   : 0.12849
	 Current Time     : 442.95 sec

  2000: 
	 Current Distance : 6.66792
	 Worst Distance   : 0.12022
	 Current Time     : 467.28 sec

  2100: 
	 Current Distance : 6.85049
	 Worst Distance   : 0.11619
	 Current Time     : 489.48 sec

  2200: 
	 Current Distance : 7.0428
	 Worst Distance   : 0.11136
	 Current Time     : 511.57 sec

  2300: 
	 Current Distance : 7.21546
	 Worst Distance   : 0.10487
	 Current Time     : 533.74 sec

  2400: 
	 Current Distance : 7.35927
	 Worst Distance   : 0.09751
	 Current Time     : 556.75 sec

  2500: 
	 Current Distance : 7.50343
	 Worst Distance   : 0.09068
	 Current Time     : 579.48 sec

  2600: 
	 Current Distance : 7.66444
	 Worst Distance   : 0.08766
	 Current Time     : 601.86 sec

  2700: 
	 Current Distance : 7.89899
	 Worst Distance   : 0.08248
	 Current Time     : 624.39 sec

  2800: 
	 Current Distance : 8.16213
	 Worst Distance   : 0.07988
	 Current Time     : 647.6 sec

  2900: 
	 Current Distance : 8.33339
	 Worst Distance   : 0.07632
	 Current Time     : 669.8 sec

  3000: 
	 Current Distance : 8.55225
	 Worst Distance   : 0.07291
	 Current Time     : 692.01 sec

  3100: 
	 Current Distance : 8.68769
	 Worst Distance   : 0.06707
	 Current Time     : 716.55 sec

  3200: 
	 Current Distance : 8.79682
	 Worst Distance   : 0.05922
	 Current Time     : 739.66 sec

  3300: 
	 Current Distance : 8.93998
	 Worst Distance   : 0.05649
	 Current Time     : 763.42 sec

  3400: 
	 Current Distance : 9.10873
	 Worst Distance   : 0.05345
	 Current Time     : 785.99 sec

  3500: 
	 Current Distance : 9.26726
	 Worst Distance   : 0.05124
	 Current Time     : 807.91 sec

  3600: 
	 Current Distance : 9.38818
	 Worst Distance   : 0.04943
	 Current Time     : 829.95 sec

  3700: 
	 Current Distance : 9.4761
	 Worst Distance   : 0.04743
	 Current Time     : 853.92 sec

  3800: 
	 Current Distance : 9.59021
	 Worst Distance   : 0.04484
	 Current Time     : 875.76 sec

  3900: 
	 Current Distance : 9.72464
	 Worst Distance   : 0.04294
	 Current Time     : 897.85 sec

  4000: 
	 Current Distance : 9.75309
	 Worst Distance   : 0.03828
	 Current Time     : 919.78 sec

  4100: 
	 Current Distance : 9.87507
	 Worst Distance   : 0.0372
	 Current Time     : 943.31 sec

  4200: 
	 Current Distance : 9.99137
	 Worst Distance   : 0.03492
	 Current Time     : 966.2 sec

  4300: 
	 Current Distance : 10.07795
	 Worst Distance   : 0.03511
	 Current Time     : 988.34 sec

  4400: 
	 Current Distance : 10.16473
	 Worst Distance   : 0.03538
	 Current Time     : 1012.63 sec

  4500: 
	 Current Distance : 10.23072
	 Worst Distance   : 0.03569
	 Current Time     : 1036.61 sec

  4600: 
	 Current Distance : 10.28529
	 Worst Distance   : 0.03479
	 Current Time     : 1058.8 sec

  4700: 
	 Current Distance : 10.33712
	 Worst Distance   : 0.03291
	 Current Time     : 1082.35 sec

  4800: 
	 Current Distance : 10.41106
	 Worst Distance   : 0.0278
	 Current Time     : 1105.63 sec

  4900: 
	 Current Distance : 10.48557
	 Worst Distance   : 0.02168
	 Current Time     : 1129.59 sec

  5000: 
	 Current Distance : 10.52699
	 Worst Distance   : 0.02019
	 Current Time     : 1153.92 sec

  5100: 
	 Current Distance : 10.55359
	 Worst Distance   : 0.02001
	 Current Time     : 1176.98 sec

  5108: 
	 Current Distance : 10.55658
	 Worst Distance   : 0.02
	 Current Time     : 1178.72 sec

Success!
  Successfully completed an elastic net with
  an accuracy of 0.02 units

 Algorithm completed on 2015-04-21 14:10:45
 Algorithm completed in 1178.692sec

0,0.69833,0.51186
1,0.69837,0.51182
2,0.69984,0.51044
3,0.75093,0.4656
4,0.75247,0.46218
5,0.76349,0.42411
6,0.76493,0.42311
7,0.87388,0.43047
8,0.87541,0.42941
9,0.91713,0.31749
10,0.88063,0.28281
11,0.91747,0.31557
12,0.9198,0.32008
13,0.95261,0.51096
14,0.95309,0.51386
15,0.95196,0.51533
16,0.87576,0.61015
17,0.8739,0.61137
18,0.76627,0.63768
19,0.76673,0.73504
20,0.87335,0.73609
21,0.87498,0.73613
22,0.87683,0.73755
23,0.99815,0.8319
24,0.99993,0.83334
25,0.99699,0.83498
26,0.79987,0.94276
27,0.79689,0.94437
28,0.79637,0.943
29,0.76464,0.85058
30,0.68715,0.9146
31,0.64314,0.86407
32,0.64227,0.86307
33,0.64314,0.86407
34,0.68714,0.91457
35,0.76404,0.84864
36,0.76557,0.84727
37,0.76559,0.84506
38,0.76487,0.73618
39,0.69017,0.6403
40,0.68888,0.63888
41,0.69016,0.63881
42,0.76426,0.63589
43,0.76531,0.57221
44,0.76462,0.5706
45,0.70002,0.51354
46,0.69606,0.51234
47,0.60917,0.52823
48,0.5478,0.58216
49,0.54588,0.5825
50,0.42303,0.55585
51,0.42133,0.5564
52,0.37698,0.63771
53,0.37625,0.63894
54,0.37753,0.63918
55,0.45182,0.65252
56,0.45265,0.65392
57,0.45382,0.77653
58,0.52858,0.77952
59,0.52912,0.86154
60,0.39204,0.99859
61,0.39204,0.9986
62,0.52916,0.8628
63,0.5312,0.85938
64,0.52993,0.77952
65,0.52858,0.77825
66,0.45289,0.77778
67,0.37445,0.7773
68,0.37243,0.77707
69,0.32082,0.76573
70,0.31891,0.76529
71,0.31675,0.76385
72,0.25071,0.68063
73,0.24811,0.67983
74,0.14067,0.72159
75,0.04793,0.72223
76,0.04822,0.72334
77,0.16954,0.83305
78,0.06411,0.91541
79,0.06255,0.91663
80,0.06412,0.91541
81,0.17048,0.83305
82,0.14093,0.72388
83,0.1421,0.72159
84,0.24783,0.6783
85,0.23458,0.59369
86,0.2344,0.59057
87,0.2344,0.59045
88,0.2344,0.59027
89,0.23439,0.58529
90,0.23429,0.44611
91,0.23424,0.4439
92,0.23217,0.44429
93,0.09436,0.47264
94,0.01684,0.5406
95,0.01569,0.54161
96,0.01682,0.54058
97,0.0928,0.47154
98,0.07814,0.3353
99,0.07673,0.33262
100,0.00141,0.29242
101,2.6E-4,0.2918
102,0.00142,0.29239
103,0.07784,0.33069
104,0.15274,0.20737
105,0.10407,0.17945
106,0.10236,0.17895
107,0.1036,0.17814
108,0.13696,0.14384
109,0.14057,0.01584
110,0.14062,0.01395
111,0.14058,0.01583
112,0.13778,0.1429
113,0.13688,0.1489
114,0.1553,0.20619
115,0.24908,0.24954
116,0.25162,0.25165
117,0.31073,0.34312
118,0.21982,0.36116
119,0.21983,0.36117
120,0.31187,0.34357
121,0.32826,0.27847
122,0.32913,0.27684
123,0.37356,0.22395
124,0.37499,0.22262
125,0.45194,0.16769
126,0.45429,0.16767
127,0.52965,0.22114
128,0.45454,0.16558
129,0.46854,0.02984
130,0.46871,0.02783
131,0.46643,0.02923
132,0.31405,0.12405
133,0.31191,0.12625
134,0.25263,0.24986
135,0.31271,0.34539
136,0.35908,0.48466
137,0.36018,0.48539
138,0.42182,0.41735
139,0.42337,0.41651
140,0.50007,0.40588
141,0.53096,0.47744
142,0.53134,0.47985
143,0.53097,0.47739
144,0.50047,0.40461
145,0.46912,0.30658
146,0.46989,0.30558
147,0.57698,0.30544
148,0.5776,0.3046
149,0.53233,0.22216
150,0.59292,0.12649
151,0.59386,0.12507
152,0.59526,0.12547
153,0.6862,0.15348
154,0.65701,0.23424
155,0.68779,0.15301
156,0.74866,0.08328
157,0.68845,0.00129
158,0.68755,7.0E-5
159,0.68847,0.00127
160,0.75011,0.08185
161,0.82774,0.01452
162,0.82942,0.01374
163,0.90713,0.00542
164,0.90866,0.00581
165,0.90968,0.00742
166,0.93579,0.05277
167,0.93631,0.05461
168,0.93733,0.13692
169,0.93732,0.13827
170,0.93592,0.13852
171,0.84474,0.15264
172,0.84303,0.15347
173,0.76677,0.22122
174,0.76725,0.2231
175,0.87602,0.27931
176,0.87599,0.27931
177,0.7656,0.22334
178,0.65809,0.23661
179,0.71486,0.30971
180,0.71532,0.31137
181,0.71439,0.3131
182,0.68928,0.35809
183,0.68789,0.35982
184,0.60991,0.43001
185,0.60828,0.43125
186,0.53301,0.48008
187,0.60902,0.52715
188,0.69601,0.51226
189,0.69827,0.51187
,,,Distance,10.55658

De-Normalizing route

DONE!


 Success in writing TSP and LOG Files
 Program terminates at 2015-04-21 14:10:45
 Program completed in 1178772.0sec


------------------
 T : 1178.0 s
 I : 5108
 D : 648.0
------------------
