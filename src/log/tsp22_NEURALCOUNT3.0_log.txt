+----------------------------+
| Sean Grogan's Elastic Net  |
+----------------------------+
Beginning: 2015-04-23 08:20:58

Paramters:
inFile     ./src/data/tsp22.txt
logFile    ./src/log/tsp22_NEURALCOUNT3.0_log.txt
outFile    ./src/out/tsp22_NEURALCOUNT3.0_out.csv
alpha      2.0
beta       0.2
initK      0.2
epsilon    0.02
KAlpha     0.99
KUpdate    25.0
NeuronsFac 3.0
radius     0.5
MaxIter    10000
DispIter   100

  0: 
	 Current Distance : 2.68298
	 Worst Distance   : 0.29679
	 Current Time     : 0.03 sec

  100: 
	 Current Distance : 2.66528
	 Worst Distance   : 0.24359
	 Current Time     : 0.84 sec

  200: 
	 Current Distance : 2.7519
	 Worst Distance   : 0.22403
	 Current Time     : 1.61 sec

  300: 
	 Current Distance : 2.84486
	 Worst Distance   : 0.19627
	 Current Time     : 2.38 sec

  400: 
	 Current Distance : 2.96238
	 Worst Distance   : 0.19092
	 Current Time     : 3.17 sec

  500: 
	 Current Distance : 3.08205
	 Worst Distance   : 0.18169
	 Current Time     : 3.91 sec

  600: 
	 Current Distance : 3.18925
	 Worst Distance   : 0.15652
	 Current Time     : 4.68 sec

  700: 
	 Current Distance : 3.33438
	 Worst Distance   : 0.13449
	 Current Time     : 5.51 sec

  800: 
	 Current Distance : 3.45611
	 Worst Distance   : 0.13296
	 Current Time     : 6.31 sec

  900: 
	 Current Distance : 3.56327
	 Worst Distance   : 0.1362
	 Current Time     : 7.1 sec

  1000: 
	 Current Distance : 3.65572
	 Worst Distance   : 0.11475
	 Current Time     : 7.85 sec

  1100: 
	 Current Distance : 3.73531
	 Worst Distance   : 0.11104
	 Current Time     : 8.65 sec

  1200: 
	 Current Distance : 3.79892
	 Worst Distance   : 0.11006
	 Current Time     : 9.43 sec

  1300: 
	 Current Distance : 3.85723
	 Worst Distance   : 0.10873
	 Current Time     : 10.16 sec

  1400: 
	 Current Distance : 3.91402
	 Worst Distance   : 0.10931
	 Current Time     : 11.03 sec

  1500: 
	 Current Distance : 3.96492
	 Worst Distance   : 0.10966
	 Current Time     : 11.94 sec

  1600: 
	 Current Distance : 4.03167
	 Worst Distance   : 0.10981
	 Current Time     : 12.73 sec

  1700: 
	 Current Distance : 4.09804
	 Worst Distance   : 0.10977
	 Current Time     : 13.52 sec

  1800: 
	 Current Distance : 4.1443
	 Worst Distance   : 0.10952
	 Current Time     : 14.29 sec

  1900: 
	 Current Distance : 4.18564
	 Worst Distance   : 0.10903
	 Current Time     : 15.03 sec

  2000: 
	 Current Distance : 4.22422
	 Worst Distance   : 0.10822
	 Current Time     : 15.81 sec

  2100: 
	 Current Distance : 4.25971
	 Worst Distance   : 0.10678
	 Current Time     : 16.58 sec

  2200: 
	 Current Distance : 4.29525
	 Worst Distance   : 0.10325
	 Current Time     : 17.35 sec

  2300: 
	 Current Distance : 4.41816
	 Worst Distance   : 0.09236
	 Current Time     : 18.14 sec

  2400: 
	 Current Distance : 4.50339
	 Worst Distance   : 0.09147
	 Current Time     : 19.01 sec

  2500: 
	 Current Distance : 4.543
	 Worst Distance   : 0.09044
	 Current Time     : 20.11 sec

  2600: 
	 Current Distance : 4.57306
	 Worst Distance   : 0.08846
	 Current Time     : 21.11 sec

  2700: 
	 Current Distance : 4.60535
	 Worst Distance   : 0.08045
	 Current Time     : 21.89 sec

  2800: 
	 Current Distance : 4.72007
	 Worst Distance   : 0.05854
	 Current Time     : 22.64 sec

  2900: 
	 Current Distance : 4.74887
	 Worst Distance   : 0.0549
	 Current Time     : 23.44 sec

  3000: 
	 Current Distance : 4.78242
	 Worst Distance   : 0.04936
	 Current Time     : 24.21 sec

  3100: 
	 Current Distance : 4.80494
	 Worst Distance   : 0.04781
	 Current Time     : 25.0 sec

  3200: 
	 Current Distance : 4.82324
	 Worst Distance   : 0.04669
	 Current Time     : 25.78 sec

  3300: 
	 Current Distance : 4.83844
	 Worst Distance   : 0.04588
	 Current Time     : 26.52 sec

  3400: 
	 Current Distance : 4.84969
	 Worst Distance   : 0.04529
	 Current Time     : 27.25 sec

  3500: 
	 Current Distance : 4.85775
	 Worst Distance   : 0.04481
	 Current Time     : 28.01 sec

  3600: 
	 Current Distance : 4.8662
	 Worst Distance   : 0.04427
	 Current Time     : 28.75 sec

  3700: 
	 Current Distance : 4.87755
	 Worst Distance   : 0.04312
	 Current Time     : 29.52 sec

  3800: 
	 Current Distance : 4.91148
	 Worst Distance   : 0.03675
	 Current Time     : 30.26 sec

  3900: 
	 Current Distance : 4.97751
	 Worst Distance   : 0.02643
	 Current Time     : 31.03 sec

  4000: 
	 Current Distance : 5.01121
	 Worst Distance   : 0.02617
	 Current Time     : 31.84 sec

  4100: 
	 Current Distance : 5.03612
	 Worst Distance   : 0.02583
	 Current Time     : 32.71 sec

  4200: 
	 Current Distance : 5.05572
	 Worst Distance   : 0.02534
	 Current Time     : 33.49 sec

  4300: 
	 Current Distance : 5.07145
	 Worst Distance   : 0.02464
	 Current Time     : 34.28 sec

  4400: 
	 Current Distance : 5.08433
	 Worst Distance   : 0.02358
	 Current Time     : 35.08 sec

  4500: 
	 Current Distance : 5.09516
	 Worst Distance   : 0.02194
	 Current Time     : 35.85 sec

  4578: 
	 Current Distance : 5.10284
	 Worst Distance   : 0.01998
	 Current Time     : 36.47 sec

Success!
  Successfully completed an elastic net with
  an accuracy of 0.01998 units

 Algorithm completed on 2015-04-23 08:21:34
 Algorithm completed in 36.457sec

0,0.97094,0.66017
1,0.97096,0.66023
2,0.97018,0.6613
3,0.9197,0.73194
4,0.91919,0.73276
5,0.91977,0.73347
6,0.96778,0.78776
7,0.96876,0.78886
8,0.96877,0.78891
9,0.96776,0.79127
10,0.86177,0.96234
11,0.85973,0.96364
12,0.64096,0.99966
13,0.6389,0.99998
14,0.6376,0.99796
15,0.50128,0.78252
16,0.50001,0.78049
17,0.50051,0.77889
18,0.5552,0.61082
19,0.55452,0.61029
20,0.39044,0.69433
21,0.38888,0.69513
22,0.38565,0.69676
23,0.04362,0.8692
24,0.0283,0.86604
25,0.02708,0.86555
26,0.02697,0.8655
27,0.02691,0.86547
28,0.02624,0.86515
29,0.01917,0.85929
30,1.2E-4,0.5992
31,1.7E-4,0.59628
32,0.02752,0.39217
33,0.02778,0.39024
34,0.02778,0.38741
35,0.02778,0.0882
36,0.0278,0.08538
37,0.03036,0.08457
38,0.30298,7.9E-4
39,0.30555,2.0E-5
40,0.30761,0.00126
41,0.52536,0.1343
42,0.36267,0.2912
43,0.36241,0.29291
44,0.49766,0.31866
45,0.49897,0.32013
46,0.47322,0.39937
47,0.47283,0.40056
48,0.47283,0.40056
49,0.47322,0.39937
50,0.49897,0.32013
51,0.49768,0.31864
52,0.36397,0.29145
53,0.52813,0.13453
54,0.74792,0.0375
55,0.75,0.0366
56,0.75232,0.03749
57,0.99768,0.13324
58,0.99998,0.13415
59,1.0,0.13584
60,1.0,0.31538
61,0.99998,0.31707
62,0.99793,0.3181
63,0.77917,0.42616
64,0.77899,0.42827
65,0.96871,0.6572
,,,Distance,5.10284

De-Normalizing route

DONE!


 Success in writing TSP and LOG Files
 Program terminates at 2015-04-23 08:21:34
 Program completed in 36502.0sec


------------------
 T : 36.0 s
 I : 4578
 D : 305.0
------------------
