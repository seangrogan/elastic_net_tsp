+----------------------------+
| Sean Grogan's Elastic Net  |
+----------------------------+
Beginning: 2015-04-22 22:13:31

Paramters:
inFile     ./src/data/tsp51.txt
logFile    ./src/log/tsp51_EPSILON0.03_log.txt
outFile    ./src/out/tsp51_EPSILON0.03_out.csv
alpha      2.0
beta       0.2
initK      0.2
epsilon    0.03
KAlpha     0.99
KUpdate    25.0
NeuronsFac 2.5
radius     0.5
MaxIter    10000
DispIter   100

  0: 
	 Current Distance : 2.33966
	 Worst Distance   : 0.33319
	 Current Time     : 0.09 sec

  100: 
	 Current Distance : 2.04281
	 Worst Distance   : 0.32812
	 Current Time     : 6.47 sec

  200: 
	 Current Distance : 2.21411
	 Worst Distance   : 0.31567
	 Current Time     : 12.76 sec

  300: 
	 Current Distance : 2.46475
	 Worst Distance   : 0.3059
	 Current Time     : 19.09 sec

  400: 
	 Current Distance : 2.66882
	 Worst Distance   : 0.29019
	 Current Time     : 25.4 sec

  500: 
	 Current Distance : 2.85519
	 Worst Distance   : 0.24954
	 Current Time     : 31.69 sec

  600: 
	 Current Distance : 3.15475
	 Worst Distance   : 0.23049
	 Current Time     : 37.96 sec

  700: 
	 Current Distance : 3.41687
	 Worst Distance   : 0.21385
	 Current Time     : 44.24 sec

  800: 
	 Current Distance : 3.63121
	 Worst Distance   : 0.20353
	 Current Time     : 50.62 sec

  900: 
	 Current Distance : 3.84255
	 Worst Distance   : 0.19863
	 Current Time     : 56.94 sec

  1000: 
	 Current Distance : 4.05134
	 Worst Distance   : 0.19774
	 Current Time     : 63.23 sec

  1100: 
	 Current Distance : 4.18543
	 Worst Distance   : 0.19604
	 Current Time     : 69.54 sec

  1200: 
	 Current Distance : 4.29473
	 Worst Distance   : 0.19028
	 Current Time     : 75.83 sec

  1300: 
	 Current Distance : 4.40592
	 Worst Distance   : 0.19199
	 Current Time     : 82.15 sec

  1400: 
	 Current Distance : 4.53897
	 Worst Distance   : 0.19431
	 Current Time     : 88.47 sec

  1500: 
	 Current Distance : 4.90287
	 Worst Distance   : 0.18979
	 Current Time     : 94.93 sec

  1600: 
	 Current Distance : 5.42552
	 Worst Distance   : 0.17131
	 Current Time     : 101.24 sec

  1700: 
	 Current Distance : 5.63834
	 Worst Distance   : 0.16684
	 Current Time     : 107.54 sec

  1800: 
	 Current Distance : 5.97796
	 Worst Distance   : 0.12744
	 Current Time     : 113.88 sec

  1900: 
	 Current Distance : 6.23357
	 Worst Distance   : 0.12384
	 Current Time     : 120.2 sec

  2000: 
	 Current Distance : 6.64237
	 Worst Distance   : 0.12455
	 Current Time     : 126.55 sec

  2100: 
	 Current Distance : 6.88011
	 Worst Distance   : 0.12384
	 Current Time     : 132.88 sec

  2200: 
	 Current Distance : 7.01459
	 Worst Distance   : 0.12186
	 Current Time     : 139.25 sec

  2300: 
	 Current Distance : 7.10956
	 Worst Distance   : 0.11756
	 Current Time     : 145.59 sec

  2400: 
	 Current Distance : 7.19517
	 Worst Distance   : 0.1114
	 Current Time     : 151.9 sec

  2500: 
	 Current Distance : 7.34295
	 Worst Distance   : 0.09016
	 Current Time     : 158.24 sec

  2600: 
	 Current Distance : 7.53916
	 Worst Distance   : 0.08424
	 Current Time     : 164.59 sec

  2700: 
	 Current Distance : 7.66488
	 Worst Distance   : 0.07649
	 Current Time     : 170.87 sec

  2800: 
	 Current Distance : 7.7965
	 Worst Distance   : 0.07024
	 Current Time     : 177.21 sec

  2900: 
	 Current Distance : 7.93409
	 Worst Distance   : 0.06476
	 Current Time     : 183.56 sec

  3000: 
	 Current Distance : 8.04882
	 Worst Distance   : 0.06128
	 Current Time     : 189.9 sec

  3100: 
	 Current Distance : 8.23754
	 Worst Distance   : 0.05576
	 Current Time     : 196.24 sec

  3200: 
	 Current Distance : 8.37801
	 Worst Distance   : 0.05945
	 Current Time     : 202.54 sec

  3300: 
	 Current Distance : 8.54775
	 Worst Distance   : 0.05136
	 Current Time     : 208.87 sec

  3400: 
	 Current Distance : 8.73059
	 Worst Distance   : 0.04824
	 Current Time     : 215.2 sec

  3500: 
	 Current Distance : 8.87923
	 Worst Distance   : 0.0454
	 Current Time     : 221.51 sec

  3600: 
	 Current Distance : 9.13463
	 Worst Distance   : 0.04317
	 Current Time     : 227.84 sec

  3700: 
	 Current Distance : 9.26837
	 Worst Distance   : 0.04023
	 Current Time     : 234.15 sec

  3800: 
	 Current Distance : 9.3723
	 Worst Distance   : 0.0344
	 Current Time     : 240.47 sec

  3839: 
	 Current Distance : 9.40619
	 Worst Distance   : 0.0299
	 Current Time     : 242.98 sec

Success!
  Successfully completed an elastic net with
  an accuracy of 0.0299 units

 Algorithm completed on 2015-04-22 22:17:34
 Algorithm completed in 242.964sec

0,0.95249,0.42819
1,0.85343,0.47884
2,0.85298,0.48284
3,0.82241,0.54016
4,0.82359,0.54096
5,0.97592,0.57375
6,0.97815,0.57408
7,0.97708,0.57572
8,0.91533,0.66383
9,0.91326,0.66497
10,0.76138,0.68098
11,0.7594,0.6813
12,0.76106,0.68322
13,0.89538,0.82818
14,0.89841,0.83142
15,0.9005,0.83343
16,0.98283,0.91333
17,0.99639,0.98021
18,0.99694,0.98315
19,0.99639,0.98021
20,0.98281,0.91336
21,0.89868,0.83522
22,0.81164,0.91927
23,0.80933,0.91911
24,0.63801,0.80925
25,0.6366,0.80959
26,0.64954,0.96766
27,0.64946,0.96976
28,0.64774,0.97032
29,0.55911,0.99752
30,0.55735,0.99802
31,0.55549,0.99683
32,0.44664,0.89439
33,0.38236,0.98002
34,0.38135,0.98137
35,0.38235,0.98
36,0.44522,0.89289
37,0.44495,0.88989
38,0.34526,0.77684
39,0.27693,0.6524
40,0.27691,0.6524
41,0.34302,0.77619
42,0.19311,0.81891
43,0.19181,0.82064
44,0.20268,0.88121
45,0.20347,0.88576
46,0.20132,0.88631
47,0.00254,0.92019
48,4.0E-5,0.92059
49,6.5E-4,0.91825
50,0.05142,0.73178
51,0.05239,0.72886
52,0.11607,0.57074
53,0.11546,0.56752
54,0.04435,0.51516
55,0.04204,0.51347
56,0.04204,0.51347
57,0.04436,0.51513
58,0.11667,0.56556
59,0.20874,0.42821
60,0.35077,0.41315
61,0.44646,0.5292
62,0.43283,0.6657
63,0.5532,0.7141
64,0.55503,0.71266
65,0.56993,0.63934
66,0.56993,0.63928
67,0.55356,0.70995
68,0.43282,0.66565
69,0.44526,0.52765
70,0.26202,0.32749
71,0.20702,0.42498
72,0.00269,0.30127
73,0.10154,0.15601
74,0.11101,0.14066
75,0.00199,0.0023
76,0.12142,0.12942
77,0.27431,0.06415
78,0.27431,0.06415
79,0.12142,0.12942
80,0.00199,0.0023
81,0.11101,0.14066
82,0.10158,0.15597
83,0.00312,0.30039
84,0.25644,0.32199
85,0.2615,0.32353
86,0.35156,0.41217
87,0.44688,0.52646
88,0.44813,0.52859
89,0.44815,0.52859
90,0.44819,0.52641
91,0.44022,0.41566
92,0.43953,0.41296
93,0.40881,0.26492
94,0.41228,0.2626
95,0.42543,0.25955
96,0.43878,0.25538
97,0.4407,0.15336
98,0.44038,0.15119
99,0.44229,0.15156
100,0.52609,0.15571
101,0.52858,0.15497
102,0.53096,0.15241
103,0.58557,0.06736
104,0.58744,0.06627
105,0.7009,0.06353
106,0.70287,0.06351
107,0.70643,0.06447
108,0.92813,0.14194
109,0.93091,0.14292
110,0.92923,0.14415
111,0.7938,0.24005
112,0.79189,0.24171
113,0.79111,0.24359
114,0.74648,0.35611
115,0.74587,0.35901
116,0.68824,0.45977
117,0.63823,0.55342
118,0.6376,0.55268
119,0.60685,0.38464
120,0.60727,0.38381
121,0.68884,0.45833
122,0.81264,0.43207
123,0.91392,0.33847
124,0.9153,0.3385
125,0.9539,0.42454
126,0.95451,0.42668
,,,Distance,9.40619

De-Normalizing route

DONE!


 Success in writing TSP and LOG Files
 Program terminates at 2015-04-22 22:17:34
 Program completed in 243023.0sec


------------------
 T : 242.0 s
 I : 3839
 D : 489.0
------------------
