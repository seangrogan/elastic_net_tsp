+----------------------------+
| Sean Grogan's Elastic Net  |
+----------------------------+
Beginning: 2015-04-22 15:29:48

Paramters:
inFile     ./src/data/tsp22.txt
logFile    ./src/log/tsp22_EPISLON0.01_log.txt
outFile    ./src/out/tsp22_EPISLON0.01_out.csv
alpha      2.0
beta       0.2
initK      0.2
epsilon    0.01
KAlpha     0.99
KUpdate    25.0
NeuronsFac 2.5
radius     0.5
MaxIter    10000
DispIter   100

  0: 
	 Current Distance : 2.59578
	 Worst Distance   : 0.27677
	 Current Time     : 0.04 sec

  100: 
	 Current Distance : 2.65812
	 Worst Distance   : 0.2324
	 Current Time     : 0.69 sec

  200: 
	 Current Distance : 2.74698
	 Worst Distance   : 0.21008
	 Current Time     : 1.33 sec

  300: 
	 Current Distance : 2.84146
	 Worst Distance   : 0.19318
	 Current Time     : 1.92 sec

  400: 
	 Current Distance : 2.95611
	 Worst Distance   : 0.18914
	 Current Time     : 2.54 sec

  500: 
	 Current Distance : 3.06897
	 Worst Distance   : 0.17946
	 Current Time     : 3.29 sec

  600: 
	 Current Distance : 3.19954
	 Worst Distance   : 0.14789
	 Current Time     : 3.88 sec

  700: 
	 Current Distance : 3.33469
	 Worst Distance   : 0.12084
	 Current Time     : 4.49 sec

  800: 
	 Current Distance : 3.45302
	 Worst Distance   : 0.13016
	 Current Time     : 5.07 sec

  900: 
	 Current Distance : 3.55618
	 Worst Distance   : 0.13541
	 Current Time     : 5.68 sec

  1000: 
	 Current Distance : 3.64488
	 Worst Distance   : 0.1388
	 Current Time     : 6.3 sec

  1100: 
	 Current Distance : 3.72038
	 Worst Distance   : 0.13609
	 Current Time     : 6.92 sec

  1200: 
	 Current Distance : 3.78682
	 Worst Distance   : 0.11248
	 Current Time     : 7.56 sec

  1300: 
	 Current Distance : 3.84479
	 Worst Distance   : 0.10897
	 Current Time     : 8.15 sec

  1400: 
	 Current Distance : 3.90443
	 Worst Distance   : 0.10948
	 Current Time     : 8.77 sec

  1500: 
	 Current Distance : 3.94968
	 Worst Distance   : 0.10975
	 Current Time     : 9.44 sec

  1600: 
	 Current Distance : 3.99538
	 Worst Distance   : 0.10982
	 Current Time     : 10.15 sec

  1700: 
	 Current Distance : 4.07399
	 Worst Distance   : 0.10975
	 Current Time     : 10.81 sec

  1800: 
	 Current Distance : 4.14886
	 Worst Distance   : 0.10947
	 Current Time     : 11.48 sec

  1900: 
	 Current Distance : 4.19114
	 Worst Distance   : 0.10892
	 Current Time     : 12.2 sec

  2000: 
	 Current Distance : 4.22965
	 Worst Distance   : 0.108
	 Current Time     : 12.85 sec

  2100: 
	 Current Distance : 4.26415
	 Worst Distance   : 0.10637
	 Current Time     : 13.55 sec

  2200: 
	 Current Distance : 4.29866
	 Worst Distance   : 0.10228
	 Current Time     : 14.34 sec

  2300: 
	 Current Distance : 4.43493
	 Worst Distance   : 0.09242
	 Current Time     : 15.05 sec

  2400: 
	 Current Distance : 4.49338
	 Worst Distance   : 0.09183
	 Current Time     : 15.65 sec

  2500: 
	 Current Distance : 4.53448
	 Worst Distance   : 0.09104
	 Current Time     : 16.37 sec

  2600: 
	 Current Distance : 4.56526
	 Worst Distance   : 0.08938
	 Current Time     : 16.97 sec

  2700: 
	 Current Distance : 4.59625
	 Worst Distance   : 0.08183
	 Current Time     : 17.58 sec

  2800: 
	 Current Distance : 4.7195
	 Worst Distance   : 0.05785
	 Current Time     : 18.21 sec

  2900: 
	 Current Distance : 4.76203
	 Worst Distance   : 0.05554
	 Current Time     : 18.83 sec

  3000: 
	 Current Distance : 4.77729
	 Worst Distance   : 0.05216
	 Current Time     : 19.44 sec

  3100: 
	 Current Distance : 4.79746
	 Worst Distance   : 0.04943
	 Current Time     : 20.1 sec

  3200: 
	 Current Distance : 4.81572
	 Worst Distance   : 0.0479
	 Current Time     : 20.72 sec

  3300: 
	 Current Distance : 4.83175
	 Worst Distance   : 0.0467
	 Current Time     : 21.3 sec

  3400: 
	 Current Distance : 4.84384
	 Worst Distance   : 0.0458
	 Current Time     : 21.88 sec

  3500: 
	 Current Distance : 4.85282
	 Worst Distance   : 0.04511
	 Current Time     : 22.44 sec

  3600: 
	 Current Distance : 4.86167
	 Worst Distance   : 0.04443
	 Current Time     : 22.99 sec

  3700: 
	 Current Distance : 4.87314
	 Worst Distance   : 0.04312
	 Current Time     : 23.55 sec

  3800: 
	 Current Distance : 4.90929
	 Worst Distance   : 0.03578
	 Current Time     : 24.13 sec

  3900: 
	 Current Distance : 4.97282
	 Worst Distance   : 0.02689
	 Current Time     : 24.74 sec

  4000: 
	 Current Distance : 5.00541
	 Worst Distance   : 0.02666
	 Current Time     : 25.38 sec

  4100: 
	 Current Distance : 5.03015
	 Worst Distance   : 0.02635
	 Current Time     : 26.04 sec

  4200: 
	 Current Distance : 5.05012
	 Worst Distance   : 0.02591
	 Current Time     : 26.62 sec

  4300: 
	 Current Distance : 5.06649
	 Worst Distance   : 0.02527
	 Current Time     : 27.3 sec

  4400: 
	 Current Distance : 5.08004
	 Worst Distance   : 0.02429
	 Current Time     : 27.9 sec

  4500: 
	 Current Distance : 5.09149
	 Worst Distance   : 0.02272
	 Current Time     : 28.52 sec

  4600: 
	 Current Distance : 5.10156
	 Worst Distance   : 0.02018
	 Current Time     : 29.11 sec

  4700: 
	 Current Distance : 5.11072
	 Worst Distance   : 0.01648
	 Current Time     : 29.71 sec

  4800: 
	 Current Distance : 5.1191
	 Worst Distance   : 0.01232
	 Current Time     : 30.32 sec

  4856: 
	 Current Distance : 5.12322
	 Worst Distance   : 0.00998
	 Current Time     : 30.68 sec

Success!
  Successfully completed an elastic net with
  an accuracy of 0.00998 units

 Algorithm completed on 2015-04-22 15:30:19
 Algorithm completed in 30.651sec

0,0.97016,0.65719
1,0.97173,0.65916
2,0.97125,0.65983
3,0.91781,0.73167
4,0.91782,0.73247
5,0.97018,0.79043
6,0.97073,0.79106
7,0.96992,0.79256
8,0.86172,0.96245
9,0.85987,0.96361
10,0.64013,0.99979
11,0.63812,0.99877
12,0.50078,0.78171
13,0.50031,0.77954
14,0.55524,0.61071
15,0.55462,0.61024
16,0.39028,0.69441
17,0.38888,0.69513
18,0.38603,0.69662
19,0.04999,0.87284
20,0.04317,0.87259
21,0.0416,0.87192
22,0.01323,0.85947
23,0.01201,0.85891
24,0.00975,0.8558
25,6.0E-5,0.59901
26,1.6E-4,0.59641
27,0.02762,0.39139
28,0.02778,0.38855
29,0.02778,0.08791
30,0.0278,0.08538
31,0.03009,0.08465
32,0.30324,7.1E-4
33,0.30555,2.0E-5
34,0.3074,0.00113
35,0.52561,0.13429
36,0.36281,0.29193
37,0.49819,0.31767
38,0.49937,0.31895
39,0.47262,0.40123
40,0.47238,0.40196
41,0.47262,0.40123
42,0.49937,0.31895
43,0.49819,0.31767
44,0.36282,0.29193
45,0.52809,0.13449
46,0.74876,0.03714
47,0.75139,0.03713
48,0.99791,0.13333
49,0.99998,0.13415
50,1.0,0.13567
51,0.99999,0.31606
52,0.99876,0.31768
53,0.77903,0.42622
54,0.77887,0.42812
,,,Distance,5.12322

De-Normalizing route

DONE!


 Success in writing TSP and LOG Files
 Program terminates at 2015-04-22 15:30:19
 Program completed in 30713.0sec


------------------
 T : 30.0 s
 I : 4856
 D : 305.0
------------------
+----------------------------+
| Sean Grogan's Elastic Net  |
+----------------------------+
Beginning: 2015-04-22 19:27:30

Paramters:
inFile     ./src/data/tsp22.txt
logFile    ./src/log/tsp22_EPISLON0.01_log.txt
outFile    ./src/out/tsp22_EPISLON0.01_out.csv
alpha      2.0
beta       0.2
initK      0.2
epsilon    0.01
KAlpha     0.99
KUpdate    25.0
NeuronsFac 2.5
radius     0.5
MaxIter    10000
DispIter   100

  0: 
	 Current Distance : 2.59578
	 Worst Distance   : 0.27677
	 Current Time     : 0.84 sec

  100: 
	 Current Distance : 2.65812
	 Worst Distance   : 0.2324
	 Current Time     : 2.33 sec

  200: 
	 Current Distance : 2.74698
	 Worst Distance   : 0.21008
	 Current Time     : 3.3 sec

  300: 
	 Current Distance : 2.84146
	 Worst Distance   : 0.19318
	 Current Time     : 4.17 sec

  400: 
	 Current Distance : 2.95611
	 Worst Distance   : 0.18914
	 Current Time     : 5.1 sec

  500: 
	 Current Distance : 3.06897
	 Worst Distance   : 0.17946
	 Current Time     : 5.92 sec

  600: 
	 Current Distance : 3.19954
	 Worst Distance   : 0.14789
	 Current Time     : 6.69 sec

  700: 
	 Current Distance : 3.33469
	 Worst Distance   : 0.12084
	 Current Time     : 7.5 sec

  800: 
	 Current Distance : 3.45302
	 Worst Distance   : 0.13016
	 Current Time     : 8.35 sec

  900: 
	 Current Distance : 3.55618
	 Worst Distance   : 0.13541
	 Current Time     : 9.17 sec

  1000: 
	 Current Distance : 3.64488
	 Worst Distance   : 0.1388
	 Current Time     : 9.97 sec

  1100: 
	 Current Distance : 3.72038
	 Worst Distance   : 0.13609
	 Current Time     : 10.78 sec

  1200: 
	 Current Distance : 3.78682
	 Worst Distance   : 0.11248
	 Current Time     : 11.67 sec

  1300: 
	 Current Distance : 3.84479
	 Worst Distance   : 0.10897
	 Current Time     : 12.53 sec

  1400: 
	 Current Distance : 3.90443
	 Worst Distance   : 0.10948
	 Current Time     : 13.33 sec

  1500: 
	 Current Distance : 3.94968
	 Worst Distance   : 0.10975
	 Current Time     : 14.15 sec

  1600: 
	 Current Distance : 3.99538
	 Worst Distance   : 0.10982
	 Current Time     : 14.93 sec

  1700: 
	 Current Distance : 4.07399
	 Worst Distance   : 0.10975
	 Current Time     : 15.75 sec

  1800: 
	 Current Distance : 4.14886
	 Worst Distance   : 0.10947
	 Current Time     : 16.52 sec

  1900: 
	 Current Distance : 4.19114
	 Worst Distance   : 0.10892
	 Current Time     : 17.28 sec

  2000: 
	 Current Distance : 4.22965
	 Worst Distance   : 0.108
	 Current Time     : 18.06 sec

  2100: 
	 Current Distance : 4.26415
	 Worst Distance   : 0.10637
	 Current Time     : 18.84 sec

  2200: 
	 Current Distance : 4.29866
	 Worst Distance   : 0.10228
	 Current Time     : 19.6 sec

  2300: 
	 Current Distance : 4.43493
	 Worst Distance   : 0.09242
	 Current Time     : 20.38 sec

  2400: 
	 Current Distance : 4.49338
	 Worst Distance   : 0.09183
	 Current Time     : 21.14 sec

  2500: 
	 Current Distance : 4.53448
	 Worst Distance   : 0.09104
	 Current Time     : 21.91 sec

  2600: 
	 Current Distance : 4.56526
	 Worst Distance   : 0.08938
	 Current Time     : 22.7 sec

  2700: 
	 Current Distance : 4.59625
	 Worst Distance   : 0.08183
	 Current Time     : 23.47 sec

  2800: 
	 Current Distance : 4.7195
	 Worst Distance   : 0.05785
	 Current Time     : 24.3 sec

  2900: 
	 Current Distance : 4.76203
	 Worst Distance   : 0.05554
	 Current Time     : 25.1 sec

  3000: 
	 Current Distance : 4.77729
	 Worst Distance   : 0.05216
	 Current Time     : 25.88 sec

  3100: 
	 Current Distance : 4.79746
	 Worst Distance   : 0.04943
	 Current Time     : 26.66 sec

  3200: 
	 Current Distance : 4.81572
	 Worst Distance   : 0.0479
	 Current Time     : 27.47 sec

  3300: 
	 Current Distance : 4.83175
	 Worst Distance   : 0.0467
	 Current Time     : 28.24 sec

  3400: 
	 Current Distance : 4.84384
	 Worst Distance   : 0.0458
	 Current Time     : 29.15 sec

  3500: 
	 Current Distance : 4.85282
	 Worst Distance   : 0.04511
	 Current Time     : 29.93 sec

  3600: 
	 Current Distance : 4.86167
	 Worst Distance   : 0.04443
	 Current Time     : 30.73 sec

  3700: 
	 Current Distance : 4.87314
	 Worst Distance   : 0.04312
	 Current Time     : 31.49 sec

  3800: 
	 Current Distance : 4.90929
	 Worst Distance   : 0.03578
	 Current Time     : 32.27 sec

  3900: 
	 Current Distance : 4.97282
	 Worst Distance   : 0.02689
	 Current Time     : 33.07 sec

  4000: 
	 Current Distance : 5.00541
	 Worst Distance   : 0.02666
	 Current Time     : 33.9 sec

  4100: 
	 Current Distance : 5.03015
	 Worst Distance   : 0.02635
	 Current Time     : 34.68 sec

  4200: 
	 Current Distance : 5.05012
	 Worst Distance   : 0.02591
	 Current Time     : 35.46 sec

  4300: 
	 Current Distance : 5.06649
	 Worst Distance   : 0.02527
	 Current Time     : 36.23 sec

  4400: 
	 Current Distance : 5.08004
	 Worst Distance   : 0.02429
	 Current Time     : 37.03 sec

  4500: 
	 Current Distance : 5.09149
	 Worst Distance   : 0.02272
	 Current Time     : 37.81 sec

  4600: 
	 Current Distance : 5.10156
	 Worst Distance   : 0.02018
	 Current Time     : 38.73 sec

  4700: 
	 Current Distance : 5.11072
	 Worst Distance   : 0.01648
	 Current Time     : 39.52 sec

  4800: 
	 Current Distance : 5.1191
	 Worst Distance   : 0.01232
	 Current Time     : 40.3 sec

  4856: 
	 Current Distance : 5.12322
	 Worst Distance   : 0.00998
	 Current Time     : 40.78 sec

Success!
  Successfully completed an elastic net with
  an accuracy of 0.00998 units

 Algorithm completed on 2015-04-22 19:28:11
 Algorithm completed in 39.982sec

0,0.97016,0.65719
1,0.97173,0.65916
2,0.97125,0.65983
3,0.91781,0.73167
4,0.91782,0.73247
5,0.97018,0.79043
6,0.97073,0.79106
7,0.96992,0.79256
8,0.86172,0.96245
9,0.85987,0.96361
10,0.64013,0.99979
11,0.63812,0.99877
12,0.50078,0.78171
13,0.50031,0.77954
14,0.55524,0.61071
15,0.55462,0.61024
16,0.39028,0.69441
17,0.38888,0.69513
18,0.38603,0.69662
19,0.04999,0.87284
20,0.04317,0.87259
21,0.0416,0.87192
22,0.01323,0.85947
23,0.01201,0.85891
24,0.00975,0.8558
25,6.0E-5,0.59901
26,1.6E-4,0.59641
27,0.02762,0.39139
28,0.02778,0.38855
29,0.02778,0.08791
30,0.0278,0.08538
31,0.03009,0.08465
32,0.30324,7.1E-4
33,0.30555,2.0E-5
34,0.3074,0.00113
35,0.52561,0.13429
36,0.36281,0.29193
37,0.49819,0.31767
38,0.49937,0.31895
39,0.47262,0.40123
40,0.47238,0.40196
41,0.47262,0.40123
42,0.49937,0.31895
43,0.49819,0.31767
44,0.36282,0.29193
45,0.52809,0.13449
46,0.74876,0.03714
47,0.75139,0.03713
48,0.99791,0.13333
49,0.99998,0.13415
50,1.0,0.13567
51,0.99999,0.31606
52,0.99876,0.31768
53,0.77903,0.42622
54,0.77887,0.42812
,,,Distance,5.12322

De-Normalizing route

DONE!


 Success in writing TSP and LOG Files
 Program terminates at 2015-04-22 19:28:11
 Program completed in 40832.0sec


------------------
 T : 39.0 s
 I : 4856
 D : 305.0
------------------
