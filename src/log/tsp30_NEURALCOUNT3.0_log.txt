+----------------------------+
| Sean Grogan's Elastic Net  |
+----------------------------+
Beginning: 2015-04-23 08:21:34

Paramters:
inFile     ./src/data/tsp30.txt
logFile    ./src/log/tsp30_NEURALCOUNT3.0_log.txt
outFile    ./src/out/tsp30_NEURALCOUNT3.0_out.csv
alpha      2.0
beta       0.2
initK      0.2
epsilon    0.02
KAlpha     0.99
KUpdate    25.0
NeuronsFac 3.0
radius     0.5
MaxIter    10000
DispIter   100

  0: 
	 Current Distance : 2.37415
	 Worst Distance   : 0.277
	 Current Time     : 0.04 sec

  100: 
	 Current Distance : 2.49943
	 Worst Distance   : 0.27487
	 Current Time     : 2.01 sec

  200: 
	 Current Distance : 2.68989
	 Worst Distance   : 0.26876
	 Current Time     : 3.94 sec

  300: 
	 Current Distance : 2.81895
	 Worst Distance   : 0.26288
	 Current Time     : 5.87 sec

  400: 
	 Current Distance : 2.90417
	 Worst Distance   : 0.25803
	 Current Time     : 7.74 sec

  500: 
	 Current Distance : 2.97271
	 Worst Distance   : 0.25337
	 Current Time     : 9.64 sec

  600: 
	 Current Distance : 3.03535
	 Worst Distance   : 0.24901
	 Current Time     : 11.5 sec

  700: 
	 Current Distance : 3.09443
	 Worst Distance   : 0.24434
	 Current Time     : 13.4 sec

  800: 
	 Current Distance : 3.1468
	 Worst Distance   : 0.24042
	 Current Time     : 15.25 sec

  900: 
	 Current Distance : 3.20395
	 Worst Distance   : 0.23687
	 Current Time     : 17.1 sec

  1000: 
	 Current Distance : 3.24817
	 Worst Distance   : 0.23361
	 Current Time     : 18.98 sec

  1100: 
	 Current Distance : 3.32082
	 Worst Distance   : 0.224
	 Current Time     : 20.83 sec

  1200: 
	 Current Distance : 3.56173
	 Worst Distance   : 0.16092
	 Current Time     : 23.28 sec

  1300: 
	 Current Distance : 3.75738
	 Worst Distance   : 0.1545
	 Current Time     : 25.17 sec

  1400: 
	 Current Distance : 3.89742
	 Worst Distance   : 0.14757
	 Current Time     : 27.11 sec

  1500: 
	 Current Distance : 3.95052
	 Worst Distance   : 0.14484
	 Current Time     : 28.98 sec

  1600: 
	 Current Distance : 4.01523
	 Worst Distance   : 0.14207
	 Current Time     : 30.91 sec

  1700: 
	 Current Distance : 4.05066
	 Worst Distance   : 0.14348
	 Current Time     : 32.81 sec

  1800: 
	 Current Distance : 4.0915
	 Worst Distance   : 0.14088
	 Current Time     : 34.75 sec

  1900: 
	 Current Distance : 4.13347
	 Worst Distance   : 0.14463
	 Current Time     : 37.56 sec

  2000: 
	 Current Distance : 4.18498
	 Worst Distance   : 0.11418
	 Current Time     : 40.69 sec

  2100: 
	 Current Distance : 4.279
	 Worst Distance   : 0.0876
	 Current Time     : 43.36 sec

  2200: 
	 Current Distance : 4.32424
	 Worst Distance   : 0.08397
	 Current Time     : 45.82 sec

  2300: 
	 Current Distance : 4.3707
	 Worst Distance   : 0.08317
	 Current Time     : 48.36 sec

  2400: 
	 Current Distance : 4.42955
	 Worst Distance   : 0.08207
	 Current Time     : 50.83 sec

  2500: 
	 Current Distance : 4.51122
	 Worst Distance   : 0.07986
	 Current Time     : 53.33 sec

  2600: 
	 Current Distance : 4.67446
	 Worst Distance   : 0.07407
	 Current Time     : 55.7 sec

  2700: 
	 Current Distance : 4.78312
	 Worst Distance   : 0.07023
	 Current Time     : 58.23 sec

  2800: 
	 Current Distance : 4.87055
	 Worst Distance   : 0.06669
	 Current Time     : 60.7 sec

  2900: 
	 Current Distance : 4.97082
	 Worst Distance   : 0.06226
	 Current Time     : 62.68 sec

  3000: 
	 Current Distance : 5.0711
	 Worst Distance   : 0.06213
	 Current Time     : 64.59 sec

  3100: 
	 Current Distance : 5.14989
	 Worst Distance   : 0.06164
	 Current Time     : 66.48 sec

  3200: 
	 Current Distance : 5.2094
	 Worst Distance   : 0.06074
	 Current Time     : 68.42 sec

  3300: 
	 Current Distance : 5.25728
	 Worst Distance   : 0.0591
	 Current Time     : 70.32 sec

  3400: 
	 Current Distance : 5.29695
	 Worst Distance   : 0.05537
	 Current Time     : 72.23 sec

  3500: 
	 Current Distance : 5.3374
	 Worst Distance   : 0.04464
	 Current Time     : 74.17 sec

  3600: 
	 Current Distance : 5.37679
	 Worst Distance   : 0.04458
	 Current Time     : 76.13 sec

  3700: 
	 Current Distance : 5.40974
	 Worst Distance   : 0.04459
	 Current Time     : 78.03 sec

  3800: 
	 Current Distance : 5.449
	 Worst Distance   : 0.04463
	 Current Time     : 79.92 sec

  3900: 
	 Current Distance : 5.51267
	 Worst Distance   : 0.04465
	 Current Time     : 81.88 sec

  4000: 
	 Current Distance : 5.56441
	 Worst Distance   : 0.04462
	 Current Time     : 83.92 sec

  4100: 
	 Current Distance : 5.60186
	 Worst Distance   : 0.04447
	 Current Time     : 86.1 sec

  4200: 
	 Current Distance : 5.63488
	 Worst Distance   : 0.04396
	 Current Time     : 88.25 sec

  4300: 
	 Current Distance : 5.67329
	 Worst Distance   : 0.04149
	 Current Time     : 90.43 sec

  4400: 
	 Current Distance : 5.72815
	 Worst Distance   : 0.03201
	 Current Time     : 92.37 sec

  4500: 
	 Current Distance : 5.76696
	 Worst Distance   : 0.03097
	 Current Time     : 94.3 sec

  4600: 
	 Current Distance : 5.80322
	 Worst Distance   : 0.02945
	 Current Time     : 96.26 sec

  4700: 
	 Current Distance : 5.82595
	 Worst Distance   : 0.02712
	 Current Time     : 98.51 sec

  4800: 
	 Current Distance : 5.84419
	 Worst Distance   : 0.02286
	 Current Time     : 100.55 sec

  4900: 
	 Current Distance : 5.8601
	 Worst Distance   : 0.02182
	 Current Time     : 102.52 sec

  5000: 
	 Current Distance : 5.87758
	 Worst Distance   : 0.02075
	 Current Time     : 104.52 sec

  5100: 
	 Current Distance : 5.92701
	 Worst Distance   : 0.0206
	 Current Time     : 106.86 sec

  5200: 
	 Current Distance : 5.96097
	 Worst Distance   : 0.02039
	 Current Time     : 110.39 sec

  5288: 
	 Current Distance : 5.98766
	 Worst Distance   : 0.02
	 Current Time     : 112.41 sec

Success!
  Successfully completed an elastic net with
  an accuracy of 0.02 units

 Algorithm completed on 2015-04-23 08:23:27
 Algorithm completed in 112.404sec

0,0.58445,0.60667
1,0.58567,0.60567
2,0.7067,0.57105
3,0.72983,0.56114
4,0.77528,0.69549
5,0.80932,0.77018
6,0.90945,0.67469
7,0.91048,0.67455
8,0.95468,0.74937
9,0.955,0.7499
10,0.95468,0.74937
11,0.90977,0.67523
12,0.80957,0.77075
13,0.80862,0.77088
14,0.77528,0.69549
15,0.72983,0.56114
16,0.7067,0.57105
17,0.58567,0.60573
18,0.58455,0.60855
19,0.59209,0.65274
20,0.61354,0.66595
21,0.61637,0.66744
22,0.61656,0.66754
23,0.61635,0.66743
24,0.61327,0.66584
25,0.59005,0.65508
26,0.3944,0.83661
27,0.43789,0.94488
28,0.43818,0.94565
29,0.4351,0.94604
30,0.0031,0.99962
31,4.0E-5,0.99999
32,0.00278,0.99884
33,0.3907,0.83655
34,0.24568,0.5907
35,0.24132,0.58679
36,0.24122,0.58672
37,0.24121,0.58671
38,0.24117,0.58669
39,0.23956,0.58539
40,0.18035,0.51162
41,0.17991,0.51109
42,0.17905,0.51184
43,0.05676,0.61905
44,0.05602,0.61884
45,0.02271,0.46847
46,0.02248,0.46739
47,0.02324,0.46662
48,0.13006,0.35939
49,0.13203,0.3587
50,0.13305,0.3587
51,0.17561,0.35874
52,0.17666,0.35876
53,0.24936,0.36055
54,0.25086,0.35934
55,0.25091,0.35924
56,0.25091,0.35923
57,0.25091,0.35923
58,0.25091,0.35923
59,0.25091,0.35923
60,0.25093,0.35919
61,0.25114,0.35868
62,0.25595,0.35217
63,0.47037,0.30467
64,0.47166,0.30365
65,0.43908,0.20645
66,0.43923,0.2053
67,0.48213,0.15341
68,0.48247,0.15299
69,0.48213,0.15341
70,0.43943,0.20603
71,0.47282,0.30362
72,0.63313,0.30097
73,0.67105,0.27347
74,0.89672,0.00259
75,0.89886,2.0E-5
76,0.89889,3.0E-5
77,0.89983,0.00317
78,0.99952,0.33537
79,0.99957,0.33736
80,0.91053,0.4235
81,0.90948,0.42381
82,0.77623,0.40232
83,0.77454,0.40128
84,0.66925,0.27627
85,0.63527,0.30092
86,0.77398,0.40256
87,0.7325,0.55778
88,0.70682,0.57098
89,0.58567,0.60567
,,,Distance,5.98766

De-Normalizing route

DONE!


 Success in writing TSP and LOG Files
 Program terminates at 2015-04-23 08:23:27
 Program completed in 112451.0sec


------------------
 T : 112.0 s
 I : 5288
 D : 455.0
------------------
