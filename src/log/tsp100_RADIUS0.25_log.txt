+----------------------------+
| Sean Grogan's Elastic Net  |
+----------------------------+
Beginning: 2015-04-23 15:15:01

Paramters:
inFile     ./src/data/tsp100.txt
logFile    ./src/log/tsp100_RADIUS0.25_log.txt
outFile    ./src/out/tsp100_RADIUS0.25_out.csv
alpha      2.0
beta       0.2
initK      0.2
epsilon    0.02
KAlpha     0.99
KUpdate    25.0
NeuronsFac 2.5
radius     0.25
MaxIter    10000
DispIter   100

  0: 
	 Current Distance : 1.8593
	 Worst Distance   : 0.36362
	 Current Time     : 0.53 sec

  100: 
	 Current Distance : 2.06034
	 Worst Distance   : 0.32723
	 Current Time     : 46.44 sec

  200: 
	 Current Distance : 2.1767
	 Worst Distance   : 0.30466
	 Current Time     : 92.33 sec

  300: 
	 Current Distance : 2.39008
	 Worst Distance   : 0.2784
	 Current Time     : 138.32 sec

  400: 
	 Current Distance : 2.51408
	 Worst Distance   : 0.27406
	 Current Time     : 184.1 sec

  500: 
	 Current Distance : 2.64507
	 Worst Distance   : 0.26984
	 Current Time     : 230.05 sec

  600: 
	 Current Distance : 2.76014
	 Worst Distance   : 0.26435
	 Current Time     : 275.8 sec

  700: 
	 Current Distance : 2.84374
	 Worst Distance   : 0.255
	 Current Time     : 321.44 sec

  800: 
	 Current Distance : 2.93239
	 Worst Distance   : 0.24391
	 Current Time     : 367.42 sec

  900: 
	 Current Distance : 3.0197
	 Worst Distance   : 0.23135
	 Current Time     : 413.29 sec

  1000: 
	 Current Distance : 3.09273
	 Worst Distance   : 0.22343
	 Current Time     : 459.02 sec

  1100: 
	 Current Distance : 3.15497
	 Worst Distance   : 0.21313
	 Current Time     : 504.73 sec

  1200: 
	 Current Distance : 3.22348
	 Worst Distance   : 0.20007
	 Current Time     : 550.62 sec

  1300: 
	 Current Distance : 3.31638
	 Worst Distance   : 0.1911
	 Current Time     : 596.37 sec

  1400: 
	 Current Distance : 3.41015
	 Worst Distance   : 0.18312
	 Current Time     : 642.05 sec

  1500: 
	 Current Distance : 3.48272
	 Worst Distance   : 0.18262
	 Current Time     : 688.06 sec

  1600: 
	 Current Distance : 3.57198
	 Worst Distance   : 0.18006
	 Current Time     : 733.77 sec

  1700: 
	 Current Distance : 3.69962
	 Worst Distance   : 0.17717
	 Current Time     : 779.56 sec

  1800: 
	 Current Distance : 3.90482
	 Worst Distance   : 0.17209
	 Current Time     : 825.38 sec

  1900: 
	 Current Distance : 4.09121
	 Worst Distance   : 0.17408
	 Current Time     : 871.13 sec

  2000: 
	 Current Distance : 4.27837
	 Worst Distance   : 0.17254
	 Current Time     : 916.83 sec

  2100: 
	 Current Distance : 4.61433
	 Worst Distance   : 0.16808
	 Current Time     : 962.58 sec

  2200: 
	 Current Distance : 4.96355
	 Worst Distance   : 0.12437
	 Current Time     : 1008.53 sec

  2300: 
	 Current Distance : 5.15359
	 Worst Distance   : 0.12515
	 Current Time     : 1054.19 sec

  2400: 
	 Current Distance : 5.35328
	 Worst Distance   : 0.12214
	 Current Time     : 1100.06 sec

  2500: 
	 Current Distance : 5.51753
	 Worst Distance   : 0.13058
	 Current Time     : 1145.94 sec

  2600: 
	 Current Distance : 5.69983
	 Worst Distance   : 0.10947
	 Current Time     : 1191.62 sec

  2700: 
	 Current Distance : 5.83596
	 Worst Distance   : 0.10836
	 Current Time     : 1237.35 sec

  2800: 
	 Current Distance : 5.92355
	 Worst Distance   : 0.10729
	 Current Time     : 1283.18 sec

  2900: 
	 Current Distance : 6.00165
	 Worst Distance   : 0.10604
	 Current Time     : 1328.83 sec

  3000: 
	 Current Distance : 6.26532
	 Worst Distance   : 0.10377
	 Current Time     : 1374.54 sec

  3100: 
	 Current Distance : 6.34713
	 Worst Distance   : 0.09661
	 Current Time     : 1420.5 sec

  3200: 
	 Current Distance : 6.50172
	 Worst Distance   : 0.09421
	 Current Time     : 1466.21 sec

  3300: 
	 Current Distance : 6.65714
	 Worst Distance   : 0.08446
	 Current Time     : 1511.87 sec

  3400: 
	 Current Distance : 6.95952
	 Worst Distance   : 0.08159
	 Current Time     : 1557.55 sec

  3500: 
	 Current Distance : 7.22355
	 Worst Distance   : 0.08062
	 Current Time     : 1603.65 sec

  3600: 
	 Current Distance : 7.41653
	 Worst Distance   : 0.07993
	 Current Time     : 4658.45 sec

  3700: 
	 Current Distance : 7.5763
	 Worst Distance   : 0.07974
	 Current Time     : 4706.58 sec

  3800: 
	 Current Distance : 7.75925
	 Worst Distance   : 0.08001
	 Current Time     : 4760.13 sec

  3900: 
	 Current Distance : 7.9552
	 Worst Distance   : 0.07982
	 Current Time     : 4816.1 sec

  4000: 
	 Current Distance : 8.11283
	 Worst Distance   : 0.07997
	 Current Time     : 4865.95 sec

  4100: 
	 Current Distance : 8.32063
	 Worst Distance   : 0.08013
	 Current Time     : 4915.95 sec

  4200: 
	 Current Distance : 8.44911
	 Worst Distance   : 0.08063
	 Current Time     : 4970.67 sec

  4300: 
	 Current Distance : 8.80723
	 Worst Distance   : 0.05381
	 Current Time     : 5023.02 sec

  4400: 
	 Current Distance : 8.92876
	 Worst Distance   : 0.05301
	 Current Time     : 5069.31 sec

  4500: 
	 Current Distance : 9.02405
	 Worst Distance   : 0.0523
	 Current Time     : 5116.52 sec

  4600: 
	 Current Distance : 9.19078
	 Worst Distance   : 0.05161
	 Current Time     : 5163.35 sec

  4700: 
	 Current Distance : 9.37052
	 Worst Distance   : 0.05088
	 Current Time     : 5210.57 sec

  4800: 
	 Current Distance : 9.48704
	 Worst Distance   : 0.04941
	 Current Time     : 5259.87 sec

  4900: 
	 Current Distance : 9.68278
	 Worst Distance   : 0.03656
	 Current Time     : 5311.52 sec

  5000: 
	 Current Distance : 9.85525
	 Worst Distance   : 0.03321
	 Current Time     : 5361.77 sec

  5100: 
	 Current Distance : 10.03568
	 Worst Distance   : 0.032
	 Current Time     : 5413.31 sec

  5200: 
	 Current Distance : 10.11718
	 Worst Distance   : 0.03195
	 Current Time     : 5467.09 sec

  5300: 
	 Current Distance : 10.18967
	 Worst Distance   : 0.03186
	 Current Time     : 5523.87 sec

  5400: 
	 Current Distance : 10.24882
	 Worst Distance   : 0.03171
	 Current Time     : 5584.46 sec

  5500: 
	 Current Distance : 10.30856
	 Worst Distance   : 0.03147
	 Current Time     : 5648.57 sec

  5600: 
	 Current Distance : 10.38053
	 Worst Distance   : 0.03101
	 Current Time     : 5717.04 sec

  5700: 
	 Current Distance : 10.43326
	 Worst Distance   : 0.02926
	 Current Time     : 5788.77 sec

  5800: 
	 Current Distance : 10.55735
	 Worst Distance   : 0.03927
	 Current Time     : 5864.68 sec

  5900: 
	 Current Distance : 10.73395
	 Worst Distance   : 0.09109
	 Current Time     : 5945.31 sec

  6000: 
	 Current Distance : 10.76985
	 Worst Distance   : 0.09296
	 Current Time     : 6029.37 sec

  6100: 
	 Current Distance : 10.79953
	 Worst Distance   : 0.09472
	 Current Time     : 6117.04 sec

  6200: 
	 Current Distance : 10.84624
	 Worst Distance   : 0.09649
	 Current Time     : 6208.05 sec

  6300: 
	 Current Distance : 10.87502
	 Worst Distance   : 0.09804
	 Current Time     : 6301.97 sec

  6400: 
	 Current Distance : 10.89767
	 Worst Distance   : 0.09943
	 Current Time     : 6399.63 sec

  6500: 
	 Current Distance : 10.91827
	 Worst Distance   : 0.10039
	 Current Time     : 6500.74 sec

  6600: 
	 Current Distance : 10.93624
	 Worst Distance   : 0.09973
	 Current Time     : 6604.61 sec

  6700: 
	 Current Distance : 10.95866
	 Worst Distance   : 0.1006
	 Current Time     : 6712.64 sec

  6800: 
	 Current Distance : 10.97862
	 Worst Distance   : 0.10142
	 Current Time     : 6824.26 sec

  6900: 
	 Current Distance : 10.99154
	 Worst Distance   : 0.10217
	 Current Time     : 6937.95 sec

  7000: 
	 Current Distance : 11.00799
	 Worst Distance   : 0.10288
	 Current Time     : 7054.52 sec

  7100: 
	 Current Distance : 11.01524
	 Worst Distance   : 0.10354
	 Current Time     : 7173.67 sec

  7200: 
	 Current Distance : 11.0406
	 Worst Distance   : 0.10416
	 Current Time     : 7295.64 sec

  7300: 
	 Current Distance : 11.05051
	 Worst Distance   : 0.10472
	 Current Time     : 7420.28 sec

  7400: 
	 Current Distance : 11.06358
	 Worst Distance   : 0.10524
	 Current Time     : 7547.14 sec

  7500: 
	 Current Distance : 11.07216
	 Worst Distance   : 0.10549
	 Current Time     : 7675.68 sec

  7600: 
	 Current Distance : 11.07193
	 Worst Distance   : 0.10549
	 Current Time     : 7804.74 sec

  7700: 
	 Current Distance : 11.07167
	 Worst Distance   : 0.10549
	 Current Time     : 7933.69 sec

  7800: 
	 Current Distance : 11.07127
	 Worst Distance   : 0.10549
	 Current Time     : 8062.64 sec

  7900: 
	 Current Distance : 11.07069
	 Worst Distance   : 0.10549
	 Current Time     : 8191.63 sec

  8000: 
	 Current Distance : 11.06982
	 Worst Distance   : 0.10549
	 Current Time     : 8320.77 sec

  8100: 
	 Current Distance : 11.06854
	 Worst Distance   : 0.10549
	 Current Time     : 8449.89 sec

  8200: 
	 Current Distance : 11.06667
	 Worst Distance   : 0.10549
	 Current Time     : 8578.93 sec

  8300: 
	 Current Distance : 11.06395
	 Worst Distance   : 0.10549
	 Current Time     : 8707.97 sec

  8400: 
	 Current Distance : 11.0601
	 Worst Distance   : 0.10549
	 Current Time     : 8837.89 sec

  8500: 
	 Current Distance : 11.05482
	 Worst Distance   : 0.10549
	 Current Time     : 8966.67 sec

  8600: 
	 Current Distance : 11.04811
	 Worst Distance   : 0.10549
	 Current Time     : 9095.5 sec

  8700: 
	 Current Distance : 11.04106
	 Worst Distance   : 0.10549
	 Current Time     : 9224.37 sec

  8800: 
	 Current Distance : 11.03873
	 Worst Distance   : 0.10549
	 Current Time     : 9353.18 sec

  8900: 
	 Current Distance : 11.04886
	 Worst Distance   : 0.10549
	 Current Time     : 9482.18 sec

  9000: 
	 Current Distance : 11.04566
	 Worst Distance   : 0.10549
	 Current Time     : 9611.02 sec

  9100: 
	 Current Distance : 11.04324
	 Worst Distance   : 0.10549
	 Current Time     : 9739.96 sec

  9200: 
	 Current Distance : 11.04433
	 Worst Distance   : 0.10549
	 Current Time     : 9868.86 sec

  9300: 
	 Current Distance : 11.04971
	 Worst Distance   : 0.10549
	 Current Time     : 9997.66 sec

  9400: 
	 Current Distance : 11.04912
	 Worst Distance   : 0.10549
	 Current Time     : 10126.53 sec

  9500: 
	 Current Distance : 11.04796
	 Worst Distance   : 0.10549
	 Current Time     : 10255.31 sec

  9600: 
	 Current Distance : 11.04634
	 Worst Distance   : 0.10549
	 Current Time     : 10384.51 sec

  9700: 
	 Current Distance : 11.0442
	 Worst Distance   : 0.10549
	 Current Time     : 10513.69 sec

  9800: 
	 Current Distance : 11.04164
	 Worst Distance   : 0.10549
	 Current Time     : 10642.91 sec

  9900: 
	 Current Distance : 11.0393
	 Worst Distance   : 0.10549
	 Current Time     : 10772.08 sec

  10000: 
	 Current Distance : 11.03914
	 Worst Distance   : 0.10549
	 Current Time     : 10901.42 sec

  10001: 
	 Current Distance : 11.03914
	 Worst Distance   : 0.10549
	 Current Time     : 10901.43 sec

Incomplete!
  Unsuccessfully completed an elastic net with
  an accuracy of 0.10549units
  (Elastic net ended because of max iter count)

 Algorithm completed on 2015-04-23 18:16:43
 Algorithm completed in 10901.402sec

0,0.74933,0.30841
1,0.77272,0.38615
2,0.77274,0.3864
3,0.76328,0.43221
4,0.7632,0.43236
5,0.73314,0.45799
6,0.73294,0.45835
7,0.69077,0.54964
8,0.69081,0.54967
9,0.75164,0.47168
10,0.73321,0.45818
11,0.75185,0.47146
12,0.75268,0.47214
13,0.89356,0.44063
14,0.83916,0.6452
15,0.99236,0.60861
16,0.99256,0.60939
17,0.96389,0.8141
18,0.96383,0.81476
19,0.95897,0.93843
20,0.95875,0.93872
21,0.85316,0.95909
22,0.85288,0.95914
23,0.8198,0.96482
24,0.81574,0.97574
25,0.81564,0.97602
26,0.81564,0.97602
27,0.81574,0.97575
28,0.81962,0.96495
29,0.81923,0.96407
30,0.83703,0.9102
31,0.75896,0.76633
32,0.75917,0.76597
33,0.75904,0.76595
34,0.71493,0.75985
35,0.7148,0.75984
36,0.71444,0.75992
37,0.59238,0.78879
38,0.5968,0.98001
39,0.5967,0.98063
40,0.55591,0.99739
41,0.55579,0.99744
42,0.55591,0.99739
43,0.59658,0.98008
44,0.55781,0.79391
45,0.59198,0.78848
46,0.55749,0.79354
47,0.44163,0.79356
48,0.44132,0.79372
49,0.40547,0.87216
50,0.40536,0.87251
51,0.40536,0.87251
52,0.40572,0.87254
53,0.50788,0.87429
54,0.50808,0.87441
55,0.50682,0.92837
56,0.50659,0.92869
57,0.50658,0.92869
58,0.50658,0.92869
59,0.50658,0.92869
60,0.50592,0.92911
61,0.44639,0.95906
62,0.4462,0.95903
63,0.41017,0.91568
64,0.40539,0.87271
65,0.40536,0.87251
66,0.40539,0.87271
67,0.40955,0.91576
68,0.13522,0.99975
69,0.1344,1.0
70,0.13426,0.99994
71,0.08918,0.98066
72,0.089,0.98058
73,0.089,0.98058
74,0.08916,0.9802
75,0.13012,0.88304
76,0.08691,0.81313
77,0.02629,0.81946
78,0.00852,0.77071
79,0.00846,0.77057
80,0.00852,0.77071
81,0.02611,0.81948
82,0.02634,0.8196
83,0.08691,0.81313
84,0.13047,0.88265
85,0.26518,0.78507
86,0.26531,0.7846
87,0.192,0.64606
88,0.19171,0.64563
89,0.19145,0.6456
90,0.10717,0.65233
91,0.13017,0.56008
92,0.09079,0.48822
93,0.04911,0.53078
94,0.04899,0.53092
95,0.04899,0.53092
96,0.04899,0.53092
97,0.04899,0.53092
98,0.04911,0.53078
99,0.09079,0.48822
100,0.13017,0.56008
101,0.10717,0.65233
102,0.19145,0.6456
103,0.19193,0.64553
104,0.27227,0.59839
105,0.27239,0.59808
106,0.24695,0.49762
107,0.24691,0.49723
108,0.24678,0.47658
109,0.24671,0.47614
110,0.24655,0.47529
111,0.23977,0.42066
112,0.3449,0.42615
113,0.34521,0.42616
114,0.3449,0.42615
115,0.23977,0.42066
116,0.24655,0.47529
117,0.24671,0.47613
118,0.24672,0.47615
119,0.24693,0.4764
120,0.27643,0.48805
121,0.36566,0.52981
122,0.41776,0.62013
123,0.42359,0.64789
124,0.42361,0.64798
125,0.42359,0.64789
126,0.41817,0.62038
127,0.41856,0.61987
128,0.55781,0.49335
129,0.55784,0.49304
130,0.43062,0.46097
131,0.43031,0.46068
132,0.39936,0.34808
133,0.38038,0.28411
134,0.45649,0.22026
135,0.38324,0.16835
136,0.33317,0.19562
137,0.29662,0.18384
138,0.31402,0.11316
139,0.24898,0.15725
140,0.24872,0.15766
141,0.22669,0.24883
142,0.18805,0.32937
143,0.04968,0.30332
144,1.8E-4,0.17284
145,0.0396,0.10654
146,0.03993,0.10649
147,0.08187,0.16029
148,0.15028,0.13755
149,0.15037,0.1374
150,0.10597,0.1236
151,0.08713,0.08892
152,0.05963,0.07179
153,0.05948,0.07145
154,0.05581,0.00574
155,0.12802,2.0E-5
156,0.12824,0.0
157,0.12802,2.0E-5
158,0.05581,0.00574
159,0.05948,0.07145
160,0.05963,0.07179
161,0.08713,0.08892
162,0.10597,0.1236
163,0.15037,0.1374
164,0.15028,0.13755
165,0.08187,0.16029
166,0.03993,0.10649
167,0.0396,0.10654
168,1.8E-4,0.17284
169,0.04968,0.30332
170,0.18805,0.32937
171,0.22669,0.24883
172,0.24886,0.15774
173,0.29645,0.18377
174,0.31419,0.11323
175,0.3332,0.19548
176,0.38303,0.1682
177,0.3835,0.16817
178,0.49094,0.18596
179,0.49108,0.18607
180,0.4567,0.22029
181,0.38033,0.28392
182,0.38016,0.2843
183,0.39938,0.34783
184,0.44077,0.33728
185,0.44088,0.33725
186,0.44088,0.33725
187,0.44088,0.33725
188,0.44084,0.33744
189,0.49724,0.39784
190,0.51787,0.34198
191,0.51822,0.34168
192,0.58636,0.25473
193,0.58656,0.25447
194,0.58657,0.25427
195,0.58989,0.18562
196,0.59004,0.18545
197,0.66129,0.1683
198,0.66188,0.16838
199,0.66188,0.16838
200,0.66169,0.16816
201,0.65276,0.11393
202,0.65969,0.05107
203,0.65944,0.05083
204,0.65943,0.05082
205,0.65943,0.05082
206,0.65944,0.05083
207,0.65969,0.05107
208,0.65276,0.11393
209,0.66169,0.16817
210,0.66261,0.16841
211,0.75592,0.12225
212,0.75614,0.12222
213,0.77323,0.15524
214,0.77348,0.15528
215,0.88617,0.14088
216,0.89533,0.05137
217,0.89553,0.05114
218,0.95366,0.06587
219,0.95383,0.06592
220,0.95366,0.06587
221,0.89551,0.05141
222,0.88645,0.14102
223,0.9112,0.22205
224,0.91101,0.24864
225,0.91144,0.22224
226,0.99965,0.22178
227,0.99999,0.22194
228,0.99719,0.26303
229,0.99718,0.26315
230,0.99719,0.26303
231,0.99999,0.22194
232,0.99965,0.22189
233,0.91127,0.24875
234,0.9109,0.24911
235,0.87356,0.31263
236,0.92205,0.34434
237,0.97976,0.34339
238,0.97999,0.34338
239,0.97999,0.34338
240,0.97976,0.34339
241,0.92205,0.34434
242,0.87345,0.31282
243,0.87293,0.31271
244,0.74998,0.30806
245,0.74926,0.30814
246,0.74925,0.30814
247,0.74925,0.30814
248,0.74925,0.30814
249,0.74925,0.30814
,,,Distance,11.03914

De-Normalizing route

DONE!


 Success in writing TSP and LOG Files
 Program terminates at 2015-04-23 18:16:43
 Program completed in 1.0901489E7sec


------------------
 T : 10901.0 s
 I : 10001
 D : 32547.0
------------------
