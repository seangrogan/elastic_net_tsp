+----------------------------+
| Sean Grogan's Elastic Net  |
+----------------------------+
Beginning: 2015-04-23 18:17:08

Paramters:
inFile     ./src/data/tsp30.txt
logFile    ./src/log/tsp30_RADIUS0.75_log.txt
outFile    ./src/out/tsp30_RADIUS0.75_out.csv
alpha      2.0
beta       0.2
initK      0.2
epsilon    0.02
KAlpha     0.99
KUpdate    25.0
NeuronsFac 2.5
radius     0.75
MaxIter    10000
DispIter   100

  0: 
	 Current Distance : 2.94363
	 Worst Distance   : 0.25286
	 Current Time     : 0.04 sec

  100: 
	 Current Distance : 2.45275
	 Worst Distance   : 0.27613
	 Current Time     : 1.39 sec

  200: 
	 Current Distance : 2.54359
	 Worst Distance   : 0.2694
	 Current Time     : 2.72 sec

  300: 
	 Current Distance : 2.60552
	 Worst Distance   : 0.26316
	 Current Time     : 4.04 sec

  400: 
	 Current Distance : 2.66387
	 Worst Distance   : 0.25806
	 Current Time     : 5.37 sec

  500: 
	 Current Distance : 2.70933
	 Worst Distance   : 0.25312
	 Current Time     : 6.66 sec

  600: 
	 Current Distance : 2.75643
	 Worst Distance   : 0.24854
	 Current Time     : 7.98 sec

  700: 
	 Current Distance : 2.80563
	 Worst Distance   : 0.24374
	 Current Time     : 9.3 sec

  800: 
	 Current Distance : 2.84778
	 Worst Distance   : 0.24046
	 Current Time     : 10.62 sec

  900: 
	 Current Distance : 2.89997
	 Worst Distance   : 0.23653
	 Current Time     : 11.93 sec

  1000: 
	 Current Distance : 2.94722
	 Worst Distance   : 0.23295
	 Current Time     : 13.25 sec

  1100: 
	 Current Distance : 3.06943
	 Worst Distance   : 0.21681
	 Current Time     : 14.58 sec

  1200: 
	 Current Distance : 3.52265
	 Worst Distance   : 0.14492
	 Current Time     : 15.88 sec

  1300: 
	 Current Distance : 3.62796
	 Worst Distance   : 0.14516
	 Current Time     : 17.2 sec

  1400: 
	 Current Distance : 3.61633
	 Worst Distance   : 0.14605
	 Current Time     : 18.52 sec

  1500: 
	 Current Distance : 3.62303
	 Worst Distance   : 0.14373
	 Current Time     : 19.83 sec

  1600: 
	 Current Distance : 3.67321
	 Worst Distance   : 0.14382
	 Current Time     : 21.13 sec

  1700: 
	 Current Distance : 3.70793
	 Worst Distance   : 0.14532
	 Current Time     : 22.42 sec

  1800: 
	 Current Distance : 3.72324
	 Worst Distance   : 0.14298
	 Current Time     : 23.75 sec

  1900: 
	 Current Distance : 3.76738
	 Worst Distance   : 0.14747
	 Current Time     : 25.07 sec

  2000: 
	 Current Distance : 3.84865
	 Worst Distance   : 0.12653
	 Current Time     : 26.38 sec

  2100: 
	 Current Distance : 3.94266
	 Worst Distance   : 0.0876
	 Current Time     : 27.71 sec

  2200: 
	 Current Distance : 3.98999
	 Worst Distance   : 0.08727
	 Current Time     : 29.05 sec

  2300: 
	 Current Distance : 4.03964
	 Worst Distance   : 0.08575
	 Current Time     : 30.37 sec

  2400: 
	 Current Distance : 4.10616
	 Worst Distance   : 0.08228
	 Current Time     : 31.68 sec

  2500: 
	 Current Distance : 4.20354
	 Worst Distance   : 0.079
	 Current Time     : 32.99 sec

  2600: 
	 Current Distance : 4.3204
	 Worst Distance   : 0.07501
	 Current Time     : 34.31 sec

  2700: 
	 Current Distance : 4.43881
	 Worst Distance   : 0.07502
	 Current Time     : 35.63 sec

  2800: 
	 Current Distance : 4.5389
	 Worst Distance   : 0.07564
	 Current Time     : 36.93 sec

  2900: 
	 Current Distance : 4.64375
	 Worst Distance   : 0.07608
	 Current Time     : 38.22 sec

  3000: 
	 Current Distance : 4.75249
	 Worst Distance   : 0.0763
	 Current Time     : 39.53 sec

  3100: 
	 Current Distance : 4.8506
	 Worst Distance   : 0.07628
	 Current Time     : 40.84 sec

  3200: 
	 Current Distance : 4.95593
	 Worst Distance   : 0.0758
	 Current Time     : 42.15 sec

  3300: 
	 Current Distance : 5.03482
	 Worst Distance   : 0.07286
	 Current Time     : 43.48 sec

  3400: 
	 Current Distance : 5.18469
	 Worst Distance   : 0.05197
	 Current Time     : 44.77 sec

  3500: 
	 Current Distance : 5.25633
	 Worst Distance   : 0.04465
	 Current Time     : 46.11 sec

  3600: 
	 Current Distance : 5.32279
	 Worst Distance   : 0.04457
	 Current Time     : 47.48 sec

  3700: 
	 Current Distance : 5.39671
	 Worst Distance   : 0.04457
	 Current Time     : 48.8 sec

  3800: 
	 Current Distance : 5.45928
	 Worst Distance   : 0.04462
	 Current Time     : 50.13 sec

  3900: 
	 Current Distance : 5.53767
	 Worst Distance   : 0.04465
	 Current Time     : 51.43 sec

  4000: 
	 Current Distance : 5.59949
	 Worst Distance   : 0.04463
	 Current Time     : 52.76 sec

  4100: 
	 Current Distance : 5.64904
	 Worst Distance   : 0.04449
	 Current Time     : 54.1 sec

  4200: 
	 Current Distance : 5.69034
	 Worst Distance   : 0.04399
	 Current Time     : 55.45 sec

  4300: 
	 Current Distance : 5.7345
	 Worst Distance   : 0.04148
	 Current Time     : 56.81 sec

  4400: 
	 Current Distance : 5.7916
	 Worst Distance   : 0.03491
	 Current Time     : 58.17 sec

  4500: 
	 Current Distance : 5.8297
	 Worst Distance   : 0.03413
	 Current Time     : 59.52 sec

  4600: 
	 Current Distance : 5.85989
	 Worst Distance   : 0.03305
	 Current Time     : 60.87 sec

  4700: 
	 Current Distance : 5.88368
	 Worst Distance   : 0.03153
	 Current Time     : 62.21 sec

  4800: 
	 Current Distance : 5.90496
	 Worst Distance   : 0.02927
	 Current Time     : 63.56 sec

  4900: 
	 Current Distance : 5.9319
	 Worst Distance   : 0.02435
	 Current Time     : 64.91 sec

  5000: 
	 Current Distance : 5.97043
	 Worst Distance   : 0.02161
	 Current Time     : 66.29 sec

  5100: 
	 Current Distance : 6.00681
	 Worst Distance   : 0.0201
	 Current Time     : 67.67 sec

  5104: 
	 Current Distance : 6.00918
	 Worst Distance   : 0.01997
	 Current Time     : 67.73 sec

Success!
  Successfully completed an elastic net with
  an accuracy of 0.01997 units

 Algorithm completed on 2015-04-23 18:18:15
 Algorithm completed in 67.712sec

0,0.5858,0.6064
1,0.71512,0.567
2,0.72334,0.56417
3,0.77537,0.69577
4,0.80913,0.77039
5,0.90991,0.67494
6,0.9545,0.74907
7,0.95486,0.74966
8,0.9545,0.74907
9,0.90991,0.67494
10,0.80913,0.77039
11,0.77533,0.69579
12,0.71493,0.5681
13,0.58586,0.6072
14,0.58892,0.6409
15,0.60971,0.66319
16,0.61353,0.66553
17,0.61383,0.66571
18,0.61378,0.66568
19,0.61271,0.66509
20,0.59735,0.65853
21,0.39453,0.8366
22,0.43798,0.94509
23,0.43796,0.9451
24,0.3915,0.83834
25,0.00299,0.99877
26,3.0E-5,0.99997
27,4.2E-4,0.99711
28,0.05572,0.62072
29,0.02264,0.46818
30,0.02265,0.46816
31,0.05664,0.61825
32,0.1797,0.51214
33,0.24071,0.58614
34,0.24186,0.58711
35,0.24188,0.58712
36,0.24186,0.58711
37,0.24071,0.58613
38,0.18065,0.51083
39,0.2467,0.36441
40,0.24948,0.35867
41,0.24791,0.35927
42,0.16937,0.35874
43,0.13263,0.3587
44,0.13263,0.3587
45,0.16937,0.35874
46,0.24791,0.35926
47,0.2496,0.35842
48,0.24965,0.35838
49,0.24965,0.35836
50,0.24979,0.35817
51,0.25425,0.35421
52,0.47062,0.30407
53,0.43943,0.2058
54,0.48064,0.1552
55,0.4811,0.15465
56,0.48064,0.1552
57,0.43944,0.2058
58,0.47257,0.30379
59,0.6363,0.29862
60,0.66868,0.27516
61,0.89712,0.0021
62,0.89887,4.0E-5
63,0.89964,0.00255
64,0.99948,0.33525
65,0.99954,0.33739
66,0.91,0.42364
67,0.77596,0.40228
68,0.77472,0.40155
69,0.66528,0.27922
70,0.63798,0.29965
71,0.72448,0.56068
72,0.71526,0.56692
73,0.5858,0.6064
74,0.58461,0.60736
,,,Distance,6.00918

De-Normalizing route

DONE!


 Success in writing TSP and LOG Files
 Program terminates at 2015-04-23 18:18:15
 Program completed in 67756.0sec


------------------
 T : 67.0 s
 I : 5104
 D : 463.0
------------------
