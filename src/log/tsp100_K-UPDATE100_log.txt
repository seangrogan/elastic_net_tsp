+----------------------------+
| Sean Grogan's Elastic Net  |
+----------------------------+
Beginning: 2015-04-23 02:17:45

Paramters:
inFile     ./src/data/tsp100.txt
logFile    ./src/log/tsp100_K-UPDATE100_log.txt
outFile    ./src/out/tsp100_K-UPDATE100_out.csv
alpha      2.0
beta       0.2
initK      0.2
epsilon    0.02
KAlpha     0.99
KUpdate    100.0
NeuronsFac 2.5
radius     0.5
MaxIter    10000
DispIter   100

  0: 
	 Current Distance : 2.40646
	 Worst Distance   : 0.31474
	 Current Time     : 0.57 sec

  100: 
	 Current Distance : 2.00468
	 Worst Distance   : 0.33514
	 Current Time     : 47.4 sec

  200: 
	 Current Distance : 2.01237
	 Worst Distance   : 0.33638
	 Current Time     : 94.12 sec

  300: 
	 Current Distance : 2.02786
	 Worst Distance   : 0.33558
	 Current Time     : 141.11 sec

  400: 
	 Current Distance : 2.04655
	 Worst Distance   : 0.33396
	 Current Time     : 187.63 sec

  500: 
	 Current Distance : 2.07018
	 Worst Distance   : 0.33092
	 Current Time     : 234.55 sec

  600: 
	 Current Distance : 2.10408
	 Worst Distance   : 0.32426
	 Current Time     : 281.51 sec

  700: 
	 Current Distance : 2.15778
	 Worst Distance   : 0.30879
	 Current Time     : 328.03 sec

  800: 
	 Current Distance : 2.22433
	 Worst Distance   : 0.29914
	 Current Time     : 374.95 sec

  900: 
	 Current Distance : 2.28329
	 Worst Distance   : 0.29503
	 Current Time     : 421.7 sec

  1000: 
	 Current Distance : 2.3325
	 Worst Distance   : 0.29133
	 Current Time     : 468.66 sec

  1100: 
	 Current Distance : 2.376
	 Worst Distance   : 0.28733
	 Current Time     : 515.51 sec

  1200: 
	 Current Distance : 2.41491
	 Worst Distance   : 0.28363
	 Current Time     : 562.35 sec

  1300: 
	 Current Distance : 2.45084
	 Worst Distance   : 0.28026
	 Current Time     : 609.07 sec

  1400: 
	 Current Distance : 2.48528
	 Worst Distance   : 0.27575
	 Current Time     : 655.87 sec

  1500: 
	 Current Distance : 2.51762
	 Worst Distance   : 0.27474
	 Current Time     : 702.64 sec

  1600: 
	 Current Distance : 2.54742
	 Worst Distance   : 0.27373
	 Current Time     : 749.68 sec

  1700: 
	 Current Distance : 2.57679
	 Worst Distance   : 0.27273
	 Current Time     : 796.5 sec

  1800: 
	 Current Distance : 2.60378
	 Worst Distance   : 0.27173
	 Current Time     : 843.38 sec

  1900: 
	 Current Distance : 2.62965
	 Worst Distance   : 0.2707
	 Current Time     : 890.43 sec

  2000: 
	 Current Distance : 2.65506
	 Worst Distance   : 0.26952
	 Current Time     : 937.27 sec

  2100: 
	 Current Distance : 2.67933
	 Worst Distance   : 0.26819
	 Current Time     : 984.09 sec

  2200: 
	 Current Distance : 2.70418
	 Worst Distance   : 0.26669
	 Current Time     : 1030.97 sec

  2300: 
	 Current Distance : 2.73082
	 Worst Distance   : 0.26487
	 Current Time     : 1077.64 sec

  2400: 
	 Current Distance : 2.75917
	 Worst Distance   : 0.26265
	 Current Time     : 1124.31 sec

  2500: 
	 Current Distance : 2.78499
	 Worst Distance   : 0.26002
	 Current Time     : 1171.13 sec

  2600: 
	 Current Distance : 2.81056
	 Worst Distance   : 0.25705
	 Current Time     : 1218.12 sec

  2700: 
	 Current Distance : 2.83602
	 Worst Distance   : 0.25388
	 Current Time     : 1266.61 sec

  2800: 
	 Current Distance : 2.86203
	 Worst Distance   : 0.25066
	 Current Time     : 1313.36 sec

  2900: 
	 Current Distance : 2.88434
	 Worst Distance   : 0.2475
	 Current Time     : 1360.18 sec

  3000: 
	 Current Distance : 2.90861
	 Worst Distance   : 0.24427
	 Current Time     : 1406.99 sec

  3100: 
	 Current Distance : 2.93027
	 Worst Distance   : 0.24098
	 Current Time     : 1453.57 sec

  3200: 
	 Current Distance : 2.95135
	 Worst Distance   : 0.23762
	 Current Time     : 1500.15 sec

  3300: 
	 Current Distance : 2.97262
	 Worst Distance   : 0.23428
	 Current Time     : 1546.89 sec

  3400: 
	 Current Distance : 2.99258
	 Worst Distance   : 0.23095
	 Current Time     : 1593.66 sec

  3500: 
	 Current Distance : 3.01323
	 Worst Distance   : 0.22762
	 Current Time     : 1640.12 sec

  3600: 
	 Current Distance : 3.03528
	 Worst Distance   : 0.22829
	 Current Time     : 1687.09 sec

  3700: 
	 Current Distance : 3.05232
	 Worst Distance   : 0.22685
	 Current Time     : 1733.94 sec

  3800: 
	 Current Distance : 3.06882
	 Worst Distance   : 0.22495
	 Current Time     : 1780.45 sec

  3900: 
	 Current Distance : 3.0843
	 Worst Distance   : 0.22309
	 Current Time     : 1827.11 sec

  4000: 
	 Current Distance : 3.10047
	 Worst Distance   : 0.22171
	 Current Time     : 1873.82 sec

  4100: 
	 Current Distance : 3.11583
	 Worst Distance   : 0.22171
	 Current Time     : 1920.25 sec

  4200: 
	 Current Distance : 3.1313
	 Worst Distance   : 0.22165
	 Current Time     : 1966.94 sec

  4300: 
	 Current Distance : 3.14766
	 Worst Distance   : 0.21742
	 Current Time     : 2013.62 sec

  4400: 
	 Current Distance : 3.1625
	 Worst Distance   : 0.21329
	 Current Time     : 2060.4 sec

  4500: 
	 Current Distance : 3.17772
	 Worst Distance   : 0.20958
	 Current Time     : 2107.27 sec

  4600: 
	 Current Distance : 3.19447
	 Worst Distance   : 0.20584
	 Current Time     : 2153.99 sec

  4700: 
	 Current Distance : 3.21283
	 Worst Distance   : 0.20197
	 Current Time     : 2200.63 sec

  4800: 
	 Current Distance : 3.23207
	 Worst Distance   : 0.19776
	 Current Time     : 2247.01 sec

  4900: 
	 Current Distance : 3.25311
	 Worst Distance   : 0.19411
	 Current Time     : 2294.0 sec

  5000: 
	 Current Distance : 3.27537
	 Worst Distance   : 0.1918
	 Current Time     : 2340.8 sec

  5100: 
	 Current Distance : 3.29821
	 Worst Distance   : 0.1905
	 Current Time     : 2387.3 sec

  5200: 
	 Current Distance : 3.32265
	 Worst Distance   : 0.18801
	 Current Time     : 2433.77 sec

  5300: 
	 Current Distance : 3.34471
	 Worst Distance   : 0.1832
	 Current Time     : 2480.51 sec

  5400: 
	 Current Distance : 3.36658
	 Worst Distance   : 0.18321
	 Current Time     : 2527.4 sec

  5500: 
	 Current Distance : 3.38621
	 Worst Distance   : 0.18317
	 Current Time     : 2574.08 sec

  5600: 
	 Current Distance : 3.40321
	 Worst Distance   : 0.18311
	 Current Time     : 2620.76 sec

  5700: 
	 Current Distance : 3.4193
	 Worst Distance   : 0.18302
	 Current Time     : 2667.46 sec

  5800: 
	 Current Distance : 3.43528
	 Worst Distance   : 0.18292
	 Current Time     : 2713.89 sec

  5900: 
	 Current Distance : 3.45205
	 Worst Distance   : 0.18279
	 Current Time     : 2760.85 sec

  6000: 
	 Current Distance : 3.47049
	 Worst Distance   : 0.1826
	 Current Time     : 2807.63 sec

  6100: 
	 Current Distance : 3.4892
	 Worst Distance   : 0.18224
	 Current Time     : 2854.32 sec

  6200: 
	 Current Distance : 3.51733
	 Worst Distance   : 0.18064
	 Current Time     : 2900.94 sec

  6300: 
	 Current Distance : 3.53803
	 Worst Distance   : 0.18026
	 Current Time     : 2947.65 sec

  6400: 
	 Current Distance : 3.56227
	 Worst Distance   : 0.1798
	 Current Time     : 2994.4 sec

  6500: 
	 Current Distance : 3.59141
	 Worst Distance   : 0.17926
	 Current Time     : 3041.02 sec

  6600: 
	 Current Distance : 3.62754
	 Worst Distance   : 0.17861
	 Current Time     : 3087.78 sec

  6700: 
	 Current Distance : 3.67662
	 Worst Distance   : 0.17788
	 Current Time     : 3134.59 sec

  6800: 
	 Current Distance : 3.73242
	 Worst Distance   : 0.17704
	 Current Time     : 3181.18 sec

  6900: 
	 Current Distance : 3.79209
	 Worst Distance   : 0.17607
	 Current Time     : 3227.72 sec

  7000: 
	 Current Distance : 3.85236
	 Worst Distance   : 0.1749
	 Current Time     : 3274.32 sec

  7100: 
	 Current Distance : 3.91007
	 Worst Distance   : 0.17349
	 Current Time     : 3321.17 sec

  7200: 
	 Current Distance : 3.96347
	 Worst Distance   : 0.17179
	 Current Time     : 3367.87 sec

  7300: 
	 Current Distance : 4.01026
	 Worst Distance   : 0.1721
	 Current Time     : 3414.77 sec

  7400: 
	 Current Distance : 4.05083
	 Worst Distance   : 0.17448
	 Current Time     : 3461.36 sec

  7500: 
	 Current Distance : 4.08363
	 Worst Distance   : 0.17331
	 Current Time     : 3507.81 sec

  7600: 
	 Current Distance : 4.1178
	 Worst Distance   : 0.17274
	 Current Time     : 3554.26 sec

  7700: 
	 Current Distance : 4.15106
	 Worst Distance   : 0.17232
	 Current Time     : 3600.82 sec

  7800: 
	 Current Distance : 4.18534
	 Worst Distance   : 0.17194
	 Current Time     : 3647.35 sec

  7900: 
	 Current Distance : 4.22948
	 Worst Distance   : 0.17149
	 Current Time     : 3694.11 sec

  8000: 
	 Current Distance : 4.39134
	 Worst Distance   : 0.17134
	 Current Time     : 3740.75 sec

  8100: 
	 Current Distance : 4.45163
	 Worst Distance   : 0.17081
	 Current Time     : 3787.43 sec

  8200: 
	 Current Distance : 4.51308
	 Worst Distance   : 0.16997
	 Current Time     : 3834.03 sec

  8300: 
	 Current Distance : 4.57125
	 Worst Distance   : 0.16863
	 Current Time     : 3880.66 sec

  8400: 
	 Current Distance : 4.62809
	 Worst Distance   : 0.16594
	 Current Time     : 3927.21 sec

  8500: 
	 Current Distance : 4.75888
	 Worst Distance   : 0.12562
	 Current Time     : 3973.72 sec

  8600: 
	 Current Distance : 4.84781
	 Worst Distance   : 0.12517
	 Current Time     : 4020.25 sec

  8700: 
	 Current Distance : 4.91992
	 Worst Distance   : 0.12473
	 Current Time     : 4066.76 sec

  8800: 
	 Current Distance : 4.9788
	 Worst Distance   : 0.12458
	 Current Time     : 4113.33 sec

  8900: 
	 Current Distance : 5.03096
	 Worst Distance   : 0.12461
	 Current Time     : 4159.89 sec

  9000: 
	 Current Distance : 5.06714
	 Worst Distance   : 0.12474
	 Current Time     : 4206.55 sec

  9100: 
	 Current Distance : 5.11452
	 Worst Distance   : 0.12478
	 Current Time     : 4253.31 sec

  9200: 
	 Current Distance : 5.1648
	 Worst Distance   : 0.12484
	 Current Time     : 4299.68 sec

  9300: 
	 Current Distance : 5.21976
	 Worst Distance   : 0.12488
	 Current Time     : 4346.47 sec

  9400: 
	 Current Distance : 5.27005
	 Worst Distance   : 0.12493
	 Current Time     : 4393.03 sec

  9500: 
	 Current Distance : 5.31838
	 Worst Distance   : 0.12474
	 Current Time     : 4439.51 sec

  9600: 
	 Current Distance : 5.36199
	 Worst Distance   : 0.12386
	 Current Time     : 4486.26 sec

  9700: 
	 Current Distance : 5.40131
	 Worst Distance   : 0.12249
	 Current Time     : 4532.97 sec

  9800: 
	 Current Distance : 5.41594
	 Worst Distance   : 0.12279
	 Current Time     : 4579.69 sec

  9900: 
	 Current Distance : 5.46493
	 Worst Distance   : 0.13244
	 Current Time     : 4628.98 sec

  10000: 
	 Current Distance : 5.49235
	 Worst Distance   : 0.11998
	 Current Time     : 4678.75 sec

  10001: 
	 Current Distance : 5.49235
	 Worst Distance   : 0.11998
	 Current Time     : 4678.77 sec

Incomplete!
  Unsuccessfully completed an elastic net with
  an accuracy of 0.11998units
  (Elastic net ended because of max iter count)

 Algorithm completed on 2015-04-23 03:35:44
 Algorithm completed in 4678.758sec

0,0.76031,0.46872
1,0.76032,0.46885
2,0.76032,0.46887
3,0.76032,0.46887
4,0.76032,0.46887
5,0.76032,0.46887
6,0.76032,0.46887
7,0.76032,0.46887
8,0.76032,0.46888
9,0.76036,0.46899
10,0.76105,0.47008
11,0.77319,0.48103
12,0.9289,0.57399
13,0.94197,0.58177
14,0.94335,0.58266
15,0.94894,0.59011
16,0.95438,0.84501
17,0.95148,0.86066
18,0.94704,0.86929
19,0.85404,0.93994
20,0.83967,0.94307
21,0.83819,0.94317
22,0.83803,0.94317
23,0.83802,0.94317
24,0.83802,0.94317
25,0.83802,0.94317
26,0.838,0.94317
27,0.8379,0.94317
28,0.83682,0.94302
29,0.82609,0.93804
30,0.72445,0.79072
31,0.72039,0.78575
32,0.72023,0.78559
33,0.72021,0.78559
34,0.71992,0.78568
35,0.71218,0.78896
36,0.52403,0.90232
37,0.49979,0.89999
38,0.49422,0.89613
39,0.49287,0.89498
40,0.49255,0.89469
41,0.49247,0.89462
42,0.49244,0.8946
43,0.49244,0.89459
44,0.49243,0.89459
45,0.49243,0.89459
46,0.49243,0.89459
47,0.49243,0.89459
48,0.49243,0.89459
49,0.49243,0.89459
50,0.49243,0.89459
51,0.49243,0.89459
52,0.49243,0.89459
53,0.49243,0.89459
54,0.49243,0.89459
55,0.49243,0.89459
56,0.49243,0.89458
57,0.49242,0.89458
58,0.49241,0.89456
59,0.49236,0.89452
60,0.49222,0.89439
61,0.4919,0.8941
62,0.49114,0.89343
63,0.48922,0.8919
64,0.48273,0.88832
65,0.44884,0.88279
66,0.12086,0.96007
67,0.11388,0.96284
68,0.11373,0.96294
69,0.11373,0.96293
70,0.11369,0.96271
71,0.11266,0.95729
72,0.07599,0.83518
73,0.06475,0.81776
74,0.06349,0.81607
75,0.06336,0.81591
76,0.06335,0.8159
77,0.06335,0.81589
78,0.06335,0.8159
79,0.06335,0.8159
80,0.06342,0.81597
81,0.06417,0.81657
82,0.07489,0.8201
83,0.25641,0.77552
84,0.25865,0.77027
85,0.18756,0.63005
86,0.18002,0.61944
87,0.17851,0.61802
88,0.17167,0.61254
89,0.11277,0.56036
90,0.10249,0.55041
91,0.10139,0.54931
92,0.10128,0.5492
93,0.10126,0.54918
94,0.10126,0.54918
95,0.10126,0.54918
96,0.10128,0.5492
97,0.10139,0.54931
98,0.10249,0.5504
99,0.11274,0.56031
100,0.17191,0.61213
101,0.18348,0.61078
102,0.26237,0.49741
103,0.26687,0.48694
104,0.26712,0.48608
105,0.26714,0.48601
106,0.26714,0.48601
107,0.26714,0.48601
108,0.26714,0.48601
109,0.26714,0.48601
110,0.26714,0.48601
111,0.26714,0.48601
112,0.26714,0.48601
113,0.26714,0.48601
114,0.26714,0.48601
115,0.26714,0.48601
116,0.26714,0.48601
117,0.26715,0.48607
118,0.26741,0.48684
119,0.2732,0.49653
120,0.40112,0.61802
121,0.40729,0.62421
122,0.40756,0.6245
123,0.40758,0.62451
124,0.40771,0.6243
125,0.41179,0.61898
126,0.5339,0.48414
127,0.53018,0.47511
128,0.44511,0.37701
129,0.44221,0.34944
130,0.45714,0.26229
131,0.44736,0.24216
132,0.32029,0.18812
133,0.30893,0.18617
134,0.30792,0.18615
135,0.30685,0.18641
136,0.29506,0.19201
137,0.17061,0.30474
138,0.08434,0.29357
139,0.08064,0.12037
140,0.08213,0.10252
141,0.08232,0.10089
142,0.08234,0.10074
143,0.08234,0.10072
144,0.08234,0.10072
145,0.08234,0.10072
146,0.08234,0.10072
147,0.08234,0.10072
148,0.08234,0.10072
149,0.08234,0.10072
150,0.08234,0.10072
151,0.08234,0.10072
152,0.08234,0.10072
153,0.08234,0.10072
154,0.08234,0.10072
155,0.08234,0.10072
156,0.08234,0.10072
157,0.08234,0.10072
158,0.08234,0.10072
159,0.08234,0.10072
160,0.08234,0.10074
161,0.08232,0.10089
162,0.08213,0.10252
163,0.08064,0.12037
164,0.08434,0.29357
165,0.17061,0.30474
166,0.29506,0.19201
167,0.30684,0.18641
168,0.30783,0.18616
169,0.30792,0.18614
170,0.30793,0.18614
171,0.30793,0.18614
172,0.30801,0.18614
173,0.30895,0.18616
174,0.32037,0.18807
175,0.44825,0.24056
176,0.46451,0.24564
177,0.45943,0.26045
178,0.44235,0.34595
179,0.44092,0.36094
180,0.44081,0.36318
181,0.4408,0.36351
182,0.44079,0.36355
183,0.44079,0.36356
184,0.44079,0.36356
185,0.44079,0.36356
186,0.44079,0.36355
187,0.4408,0.3635
188,0.44081,0.36316
189,0.44093,0.36081
190,0.44255,0.34508
191,0.4629,0.2566
192,0.4842,0.23871
193,0.54451,0.22058
194,0.65705,0.14938
195,0.66899,0.13859
196,0.67,0.13761
197,0.67008,0.13752
198,0.67009,0.13751
199,0.67009,0.13751
200,0.67009,0.13751
201,0.67009,0.13751
202,0.67009,0.13751
203,0.67009,0.13751
204,0.67009,0.13751
205,0.67009,0.13751
206,0.67009,0.13751
207,0.67009,0.13751
208,0.67009,0.13751
209,0.67009,0.13751
210,0.67009,0.13751
211,0.67016,0.13745
212,0.67109,0.1369
213,0.68441,0.13258
214,0.89699,0.09965
215,0.90609,0.09582
216,0.90647,0.09557
217,0.90648,0.09556
218,0.90648,0.09556
219,0.90648,0.09558
220,0.9064,0.09592
221,0.90594,0.10333
222,0.93038,0.25818
223,0.93148,0.27074
224,0.93149,0.27164
225,0.93148,0.2717
226,0.93148,0.27171
227,0.93148,0.27171
228,0.93148,0.27171
229,0.93148,0.27171
230,0.93148,0.27171
231,0.93148,0.27171
232,0.93148,0.27171
233,0.93148,0.27171
234,0.93148,0.27171
235,0.93148,0.27171
236,0.93148,0.27171
237,0.93148,0.27171
238,0.93148,0.27171
239,0.93148,0.27171
240,0.93143,0.27175
241,0.93072,0.27223
242,0.91941,0.27801
243,0.76693,0.33709
244,0.76266,0.34116
245,0.76255,0.34153
246,0.76257,0.34225
247,0.76268,0.35114
248,0.76018,0.45352
249,0.76025,0.46736
,,,Distance,5.49235

De-Normalizing route

DONE!


 Success in writing TSP and LOG Files
 Program terminates at 2015-04-23 03:35:44
 Program completed in 4678836.0sec


------------------
 T : 4678.0 s
 I : 10001
 D : 16573.0
------------------
