+----------------------------+
| Sean Grogan's Elastic Net  |
+----------------------------+
Beginning: 2015-04-21 21:16:19

Paramters:
inFile     ./src/data/tsp22.txt
logFile    ./src/log/tsp22_BETA0.3_log.txt
outFile    ./src/out/tsp22_BETA0.3_out.csv
alpha      2.0
beta       0.3
initK      0.2
epsilon    0.02
KAlpha     0.99
KUpdate    25.0
NeuronsFac 2.5
radius     0.5
MaxIter    10000
DispIter   100

  0: 
	 Current Distance : 2.59497
	 Worst Distance   : 0.27664
	 Current Time     : 0.03 sec

  100: 
	 Current Distance : 2.67397
	 Worst Distance   : 0.2346
	 Current Time     : 0.7 sec

  200: 
	 Current Distance : 2.76428
	 Worst Distance   : 0.21433
	 Current Time     : 1.44 sec

+----------------------------+
| Sean Grogan's Elastic Net  |
+----------------------------+
Beginning: 2015-04-21 22:06:59

Paramters:
inFile     ./src/data/tsp22.txt
logFile    ./src/log/tsp22_BETA0.3_log.txt
outFile    ./src/out/tsp22_BETA0.3_out.csv
alpha      2.0
beta       0.3
initK      0.2
epsilon    0.02
KAlpha     0.99
KUpdate    25.0
NeuronsFac 2.5
radius     0.5
MaxIter    10000
DispIter   100

  0: 
	 Current Distance : 2.59497
	 Worst Distance   : 0.27664
	 Current Time     : 0.47 sec

  100: 
	 Current Distance : 2.67397
	 Worst Distance   : 0.2346
	 Current Time     : 2.97 sec

  200: 
	 Current Distance : 2.76428
	 Worst Distance   : 0.21433
	 Current Time     : 4.99 sec

  300: 
	 Current Distance : 2.86129
	 Worst Distance   : 0.18379
	 Current Time     : 7.45 sec

  400: 
	 Current Distance : 2.96851
	 Worst Distance   : 0.17684
	 Current Time     : 9.89 sec

  500: 
	 Current Distance : 3.08593
	 Worst Distance   : 0.16368
	 Current Time     : 12.0 sec

  600: 
	 Current Distance : 3.21673
	 Worst Distance   : 0.13363
	 Current Time     : 14.9 sec

  700: 
	 Current Distance : 3.34329
	 Worst Distance   : 0.11808
	 Current Time     : 16.82 sec

  800: 
	 Current Distance : 3.45858
	 Worst Distance   : 0.12143
	 Current Time     : 18.93 sec

  900: 
	 Current Distance : 3.56551
	 Worst Distance   : 0.12752
	 Current Time     : 21.27 sec

  1000: 
	 Current Distance : 3.6556
	 Worst Distance   : 0.12867
	 Current Time     : 23.42 sec

  1100: 
	 Current Distance : 3.73275
	 Worst Distance   : 0.11016
	 Current Time     : 26.02 sec

  1200: 
	 Current Distance : 3.79725
	 Worst Distance   : 0.10849
	 Current Time     : 28.12 sec

  1300: 
	 Current Distance : 3.85836
	 Worst Distance   : 0.10808
	 Current Time     : 30.53 sec

  1400: 
	 Current Distance : 3.8932
	 Worst Distance   : 0.13052
	 Current Time     : 32.78 sec

  1500: 
	 Current Distance : 3.95037
	 Worst Distance   : 0.10836
	 Current Time     : 35.22 sec

  1600: 
	 Current Distance : 3.99809
	 Worst Distance   : 0.10844
	 Current Time     : 37.58 sec

  1700: 
	 Current Distance : 4.04389
	 Worst Distance   : 0.10825
	 Current Time     : 39.62 sec

  1800: 
	 Current Distance : 4.1362
	 Worst Distance   : 0.10788
	 Current Time     : 41.92 sec

  1900: 
	 Current Distance : 4.18285
	 Worst Distance   : 0.10715
	 Current Time     : 44.28 sec

  2000: 
	 Current Distance : 4.22395
	 Worst Distance   : 0.10593
	 Current Time     : 46.87 sec

  2100: 
	 Current Distance : 4.26118
	 Worst Distance   : 0.10372
	 Current Time     : 49.05 sec

  2200: 
	 Current Distance : 4.30086
	 Worst Distance   : 0.09789
	 Current Time     : 51.03 sec

  2300: 
	 Current Distance : 4.41959
	 Worst Distance   : 0.09196
	 Current Time     : 53.19 sec

  2400: 
	 Current Distance : 4.48074
	 Worst Distance   : 0.09129
	 Current Time     : 55.51 sec

  2500: 
	 Current Distance : 4.52298
	 Worst Distance   : 0.09034
	 Current Time     : 57.59 sec

  2600: 
	 Current Distance : 4.5542
	 Worst Distance   : 0.08837
	 Current Time     : 60.04 sec

  2700: 
	 Current Distance : 4.58543
	 Worst Distance   : 0.07969
	 Current Time     : 62.41 sec

  2800: 
	 Current Distance : 4.64837
	 Worst Distance   : 0.05803
	 Current Time     : 64.69 sec

  2900: 
	 Current Distance : 4.66734
	 Worst Distance   : 0.05126
	 Current Time     : 67.07 sec

  3000: 
	 Current Distance : 4.67306
	 Worst Distance   : 0.04905
	 Current Time     : 69.32 sec

  3100: 
	 Current Distance : 4.68356
	 Worst Distance   : 0.04727
	 Current Time     : 71.37 sec

  3200: 
	 Current Distance : 4.69452
	 Worst Distance   : 0.0458
	 Current Time     : 73.35 sec

  3300: 
	 Current Distance : 4.70491
	 Worst Distance   : 0.04455
	 Current Time     : 75.69 sec

  3400: 
	 Current Distance : 4.7135
	 Worst Distance   : 0.0434
	 Current Time     : 78.12 sec

  3500: 
	 Current Distance : 4.72067
	 Worst Distance   : 0.04216
	 Current Time     : 80.58 sec

  3600: 
	 Current Distance : 4.72861
	 Worst Distance   : 0.0405
	 Current Time     : 82.51 sec

  3700: 
	 Current Distance : 4.73975
	 Worst Distance   : 0.03762
	 Current Time     : 84.52 sec

  3800: 
	 Current Distance : 4.7663
	 Worst Distance   : 0.02962
	 Current Time     : 86.42 sec

  3900: 
	 Current Distance : 4.80833
	 Worst Distance   : 0.02544
	 Current Time     : 88.54 sec

  4000: 
	 Current Distance : 4.83617
	 Worst Distance   : 0.02512
	 Current Time     : 90.77 sec

  4100: 
	 Current Distance : 4.85709
	 Worst Distance   : 0.02468
	 Current Time     : 93.09 sec

  4200: 
	 Current Distance : 4.87371
	 Worst Distance   : 0.02408
	 Current Time     : 95.06 sec

  4300: 
	 Current Distance : 4.88736
	 Worst Distance   : 0.02324
	 Current Time     : 97.35 sec

  4400: 
	 Current Distance : 4.89887
	 Worst Distance   : 0.02202
	 Current Time     : 99.59 sec

  4500: 
	 Current Distance : 4.90884
	 Worst Distance   : 0.02024
	 Current Time     : 101.88 sec

  4507: 
	 Current Distance : 4.90943
	 Worst Distance   : 0.01999
	 Current Time     : 102.25 sec

Success!
  Successfully completed an elastic net with
  an accuracy of 0.01999 units

 Algorithm completed on 2015-04-21 22:08:41
 Algorithm completed in 101.889sec

0,0.9679,0.65743
1,0.97037,0.66092
2,0.96946,0.66229
3,0.92015,0.73201
4,0.92021,0.73362
5,0.96585,0.78559
6,0.96721,0.78718
7,0.96632,0.79009
8,0.8621,0.96175
9,0.859,0.96375
10,0.641,0.99963
11,0.63758,0.9979
12,0.50133,0.78257
13,0.50053,0.77887
14,0.55501,0.61139
15,0.55397,0.61058
16,0.39127,0.69393
17,0.38887,0.69515
18,0.38393,0.69763
19,0.04453,0.86844
20,0.02812,0.86593
21,0.02689,0.86546
22,0.02675,0.8654
23,0.02611,0.86507
24,0.01939,0.85852
25,1.9E-4,0.60006
26,2.7E-4,0.59561
27,0.02751,0.39219
28,0.02778,0.38736
29,0.02778,0.0897
30,0.02783,0.08541
31,0.03171,0.08416
32,0.30162,0.00121
33,0.30554,4.0E-5
34,0.30869,0.00193
35,0.52407,0.13439
36,0.36271,0.29116
37,0.36244,0.29294
38,0.49674,0.3206
39,0.47344,0.3986
40,0.47302,0.39999
41,0.47347,0.39861
42,0.49874,0.32086
43,0.4998,0.31631
44,0.52962,0.13497
45,0.74684,0.038
46,0.75001,0.03663
47,0.75354,0.03797
48,0.99646,0.13276
49,0.99995,0.13416
50,1.0,0.13674
51,0.99998,0.31535
52,0.99788,0.31811
53,0.77992,0.4258
54,0.77963,0.42904
,,,Distance,4.90943

De-Normalizing route

DONE!


 Success in writing TSP and LOG Files
 Program terminates at 2015-04-21 22:08:42
 Program completed in 102429.0sec


------------------
 T : 101.0 s
 I : 4507
 D : 305.0
------------------
