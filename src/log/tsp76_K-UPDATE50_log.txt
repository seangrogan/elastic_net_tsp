+----------------------------+
| Sean Grogan's Elastic Net  |
+----------------------------+
Beginning: 2015-04-22 23:37:17

Paramters:
inFile     ./src/data/tsp76.txt
logFile    ./src/log/tsp76_K-UPDATE50_log.txt
outFile    ./src/out/tsp76_K-UPDATE50_out.csv
alpha      2.0
beta       0.2
initK      0.2
epsilon    0.02
KAlpha     0.99
KUpdate    50.0
NeuronsFac 2.5
radius     0.5
MaxIter    10000
DispIter   100

  0: 
	 Current Distance : 2.16816
	 Worst Distance   : 0.30699
	 Current Time     : 0.28 sec

  100: 
	 Current Distance : 1.69693
	 Worst Distance   : 0.36288
	 Current Time     : 20.88 sec

  200: 
	 Current Distance : 1.73624
	 Worst Distance   : 0.36039
	 Current Time     : 41.5 sec

  300: 
	 Current Distance : 1.79009
	 Worst Distance   : 0.35629
	 Current Time     : 61.97 sec

  400: 
	 Current Distance : 1.85709
	 Worst Distance   : 0.35065
	 Current Time     : 82.54 sec

  500: 
	 Current Distance : 1.94143
	 Worst Distance   : 0.34274
	 Current Time     : 103.06 sec

  600: 
	 Current Distance : 2.04952
	 Worst Distance   : 0.33252
	 Current Time     : 123.58 sec

  700: 
	 Current Distance : 2.18994
	 Worst Distance   : 0.32812
	 Current Time     : 144.13 sec

  800: 
	 Current Distance : 2.31921
	 Worst Distance   : 0.32668
	 Current Time     : 164.68 sec

  900: 
	 Current Distance : 2.41274
	 Worst Distance   : 0.32065
	 Current Time     : 185.2 sec

  1000: 
	 Current Distance : 2.49078
	 Worst Distance   : 0.31009
	 Current Time     : 205.8 sec

  1100: 
	 Current Distance : 2.57608
	 Worst Distance   : 0.29755
	 Current Time     : 226.38 sec

  1200: 
	 Current Distance : 2.70111
	 Worst Distance   : 0.28902
	 Current Time     : 247.06 sec

  1300: 
	 Current Distance : 2.86406
	 Worst Distance   : 0.28367
	 Current Time     : 267.77 sec

  1400: 
	 Current Distance : 3.02187
	 Worst Distance   : 0.27803
	 Current Time     : 288.46 sec

  1500: 
	 Current Distance : 3.14221
	 Worst Distance   : 0.2715
	 Current Time     : 308.99 sec

  1600: 
	 Current Distance : 3.29991
	 Worst Distance   : 0.26427
	 Current Time     : 329.61 sec

  1700: 
	 Current Distance : 3.47959
	 Worst Distance   : 0.25714
	 Current Time     : 350.17 sec

  1800: 
	 Current Distance : 3.66173
	 Worst Distance   : 0.25299
	 Current Time     : 370.72 sec

  1900: 
	 Current Distance : 3.8377
	 Worst Distance   : 0.24835
	 Current Time     : 391.28 sec

  2000: 
	 Current Distance : 3.98697
	 Worst Distance   : 0.24374
	 Current Time     : 411.75 sec

  2100: 
	 Current Distance : 4.09897
	 Worst Distance   : 0.23924
	 Current Time     : 432.36 sec

  2200: 
	 Current Distance : 4.1849
	 Worst Distance   : 0.23502
	 Current Time     : 452.91 sec

  2300: 
	 Current Distance : 4.27535
	 Worst Distance   : 0.23041
	 Current Time     : 473.5 sec

  2400: 
	 Current Distance : 4.38226
	 Worst Distance   : 0.22581
	 Current Time     : 494.03 sec

  2500: 
	 Current Distance : 4.52089
	 Worst Distance   : 0.22062
	 Current Time     : 514.68 sec

  2600: 
	 Current Distance : 4.64633
	 Worst Distance   : 0.21476
	 Current Time     : 535.25 sec

  2700: 
	 Current Distance : 4.74521
	 Worst Distance   : 0.21206
	 Current Time     : 555.83 sec

  2800: 
	 Current Distance : 4.83767
	 Worst Distance   : 0.20843
	 Current Time     : 576.37 sec

  2900: 
	 Current Distance : 4.9343
	 Worst Distance   : 0.19201
	 Current Time     : 596.97 sec

  3000: 
	 Current Distance : 5.08264
	 Worst Distance   : 0.1812
	 Current Time     : 617.6 sec

  3100: 
	 Current Distance : 5.18728
	 Worst Distance   : 0.17813
	 Current Time     : 638.43 sec

  3200: 
	 Current Distance : 5.44856
	 Worst Distance   : 0.17576
	 Current Time     : 659.0 sec

  3300: 
	 Current Distance : 5.52535
	 Worst Distance   : 0.17431
	 Current Time     : 679.75 sec

  3400: 
	 Current Distance : 5.50033
	 Worst Distance   : 0.17324
	 Current Time     : 700.29 sec

  3500: 
	 Current Distance : 5.55875
	 Worst Distance   : 0.17098
	 Current Time     : 720.85 sec

  3600: 
	 Current Distance : 5.65237
	 Worst Distance   : 0.16077
	 Current Time     : 741.52 sec

  3700: 
	 Current Distance : 5.89106
	 Worst Distance   : 0.13226
	 Current Time     : 762.12 sec

  3800: 
	 Current Distance : 5.97986
	 Worst Distance   : 0.1269
	 Current Time     : 782.66 sec

  3900: 
	 Current Distance : 6.07951
	 Worst Distance   : 0.12559
	 Current Time     : 803.18 sec

  4000: 
	 Current Distance : 6.28051
	 Worst Distance   : 0.12405
	 Current Time     : 823.75 sec

  4100: 
	 Current Distance : 6.41197
	 Worst Distance   : 0.12068
	 Current Time     : 844.34 sec

  4200: 
	 Current Distance : 6.53223
	 Worst Distance   : 0.11878
	 Current Time     : 864.91 sec

  4300: 
	 Current Distance : 6.65336
	 Worst Distance   : 0.11619
	 Current Time     : 885.51 sec

  4400: 
	 Current Distance : 6.75681
	 Worst Distance   : 0.11167
	 Current Time     : 906.06 sec

  4500: 
	 Current Distance : 6.85113
	 Worst Distance   : 0.1068
	 Current Time     : 926.6 sec

  4600: 
	 Current Distance : 6.92251
	 Worst Distance   : 0.10466
	 Current Time     : 947.14 sec

  4700: 
	 Current Distance : 6.98281
	 Worst Distance   : 0.10203
	 Current Time     : 967.73 sec

  4800: 
	 Current Distance : 7.04166
	 Worst Distance   : 0.09797
	 Current Time     : 988.28 sec

  4900: 
	 Current Distance : 7.10169
	 Worst Distance   : 0.09438
	 Current Time     : 1008.79 sec

  5000: 
	 Current Distance : 7.16643
	 Worst Distance   : 0.09135
	 Current Time     : 1029.29 sec

  5100: 
	 Current Distance : 7.24695
	 Worst Distance   : 0.09191
	 Current Time     : 1049.85 sec

  5200: 
	 Current Distance : 7.34284
	 Worst Distance   : 0.09156
	 Current Time     : 1070.39 sec

  5300: 
	 Current Distance : 7.4549
	 Worst Distance   : 0.09161
	 Current Time     : 1090.93 sec

  5400: 
	 Current Distance : 7.6202
	 Worst Distance   : 0.09157
	 Current Time     : 1111.66 sec

  5500: 
	 Current Distance : 7.71901
	 Worst Distance   : 0.09054
	 Current Time     : 1132.28 sec

  5600: 
	 Current Distance : 7.77528
	 Worst Distance   : 0.08912
	 Current Time     : 1152.9 sec

  5700: 
	 Current Distance : 7.95939
	 Worst Distance   : 0.08687
	 Current Time     : 1173.48 sec

  5800: 
	 Current Distance : 8.09005
	 Worst Distance   : 0.08325
	 Current Time     : 1194.17 sec

  5900: 
	 Current Distance : 8.18918
	 Worst Distance   : 0.08308
	 Current Time     : 1214.76 sec

  6000: 
	 Current Distance : 8.3013
	 Worst Distance   : 0.08257
	 Current Time     : 1235.31 sec

  6100: 
	 Current Distance : 8.44187
	 Worst Distance   : 0.08181
	 Current Time     : 1255.9 sec

  6200: 
	 Current Distance : 8.53669
	 Worst Distance   : 0.08053
	 Current Time     : 1276.48 sec

  6300: 
	 Current Distance : 8.60833
	 Worst Distance   : 0.07802
	 Current Time     : 1297.03 sec

  6400: 
	 Current Distance : 8.6718
	 Worst Distance   : 0.07405
	 Current Time     : 1317.57 sec

  6500: 
	 Current Distance : 8.72358
	 Worst Distance   : 0.06967
	 Current Time     : 1338.19 sec

  6600: 
	 Current Distance : 8.78276
	 Worst Distance   : 0.06615
	 Current Time     : 1358.74 sec

  6700: 
	 Current Distance : 8.85425
	 Worst Distance   : 0.06261
	 Current Time     : 1379.38 sec

  6800: 
	 Current Distance : 8.96529
	 Worst Distance   : 0.06002
	 Current Time     : 1399.92 sec

  6900: 
	 Current Distance : 9.09088
	 Worst Distance   : 0.06047
	 Current Time     : 1420.54 sec

  7000: 
	 Current Distance : 9.16917
	 Worst Distance   : 0.06082
	 Current Time     : 1441.1 sec

  7100: 
	 Current Distance : 9.25022
	 Worst Distance   : 0.06105
	 Current Time     : 1461.64 sec

  7200: 
	 Current Distance : 9.42748
	 Worst Distance   : 0.06063
	 Current Time     : 1482.24 sec

  7300: 
	 Current Distance : 9.50634
	 Worst Distance   : 0.06055
	 Current Time     : 1502.81 sec

  7400: 
	 Current Distance : 9.59243
	 Worst Distance   : 0.06026
	 Current Time     : 1523.31 sec

  7500: 
	 Current Distance : 9.70322
	 Worst Distance   : 0.05956
	 Current Time     : 1543.9 sec

  7600: 
	 Current Distance : 9.79744
	 Worst Distance   : 0.05737
	 Current Time     : 1564.61 sec

  7700: 
	 Current Distance : 9.93239
	 Worst Distance   : 0.04762
	 Current Time     : 1585.15 sec

  7800: 
	 Current Distance : 10.07232
	 Worst Distance   : 0.04649
	 Current Time     : 1605.81 sec

  7900: 
	 Current Distance : 10.15926
	 Worst Distance   : 0.04466
	 Current Time     : 1626.54 sec

  8000: 
	 Current Distance : 10.24644
	 Worst Distance   : 0.04157
	 Current Time     : 1647.05 sec

  8100: 
	 Current Distance : 10.37184
	 Worst Distance   : 0.04181
	 Current Time     : 1667.66 sec

  8200: 
	 Current Distance : 10.45975
	 Worst Distance   : 0.04203
	 Current Time     : 1688.25 sec

  8300: 
	 Current Distance : 10.54092
	 Worst Distance   : 0.04222
	 Current Time     : 1708.81 sec

  8400: 
	 Current Distance : 10.61949
	 Worst Distance   : 0.04238
	 Current Time     : 1729.33 sec

  8500: 
	 Current Distance : 10.69436
	 Worst Distance   : 0.0425
	 Current Time     : 1749.86 sec

  8600: 
	 Current Distance : 10.77149
	 Worst Distance   : 0.03974
	 Current Time     : 1770.4 sec

  8700: 
	 Current Distance : 10.84361
	 Worst Distance   : 0.03711
	 Current Time     : 1790.9 sec

  8800: 
	 Current Distance : 10.90942
	 Worst Distance   : 0.03672
	 Current Time     : 1811.42 sec

  8900: 
	 Current Distance : 10.96333
	 Worst Distance   : 0.03615
	 Current Time     : 1832.01 sec

  9000: 
	 Current Distance : 11.01034
	 Worst Distance   : 0.03623
	 Current Time     : 1852.62 sec

  9100: 
	 Current Distance : 11.05184
	 Worst Distance   : 0.03615
	 Current Time     : 1873.21 sec

  9200: 
	 Current Distance : 11.08935
	 Worst Distance   : 0.03581
	 Current Time     : 1893.84 sec

  9300: 
	 Current Distance : 11.12494
	 Worst Distance   : 0.03509
	 Current Time     : 1914.47 sec

  9400: 
	 Current Distance : 11.15763
	 Worst Distance   : 0.03431
	 Current Time     : 1935.14 sec

  9500: 
	 Current Distance : 11.18926
	 Worst Distance   : 0.03306
	 Current Time     : 1955.93 sec

  9600: 
	 Current Distance : 11.23365
	 Worst Distance   : 0.02937
	 Current Time     : 1976.76 sec

  9700: 
	 Current Distance : 11.26689
	 Worst Distance   : 0.02599
	 Current Time     : 1997.87 sec

  9800: 
	 Current Distance : 11.29539
	 Worst Distance   : 0.02261
	 Current Time     : 2018.88 sec

  9900: 
	 Current Distance : 11.31689
	 Worst Distance   : 0.0202
	 Current Time     : 2040.1 sec

  10000: 
	 Current Distance : 11.32157
	 Worst Distance   : 0.02009
	 Current Time     : 2061.68 sec

  10001: 
	 Current Distance : 11.32157
	 Worst Distance   : 0.02009
	 Current Time     : 2061.69 sec

Incomplete!
  Unsuccessfully completed an elastic net with
  an accuracy of 0.02009units
  (Elastic net ended because of max iter count)

 Algorithm completed on 2015-04-23 00:11:39
 Algorithm completed in 2061.678sec

0,0.69891,0.50968
1,0.70044,0.51082
2,0.70047,0.51083
3,0.70047,0.51083
4,0.70047,0.51083
5,0.7005,0.5108
6,0.7018,0.5096
7,0.7529,0.45979
8,0.76261,0.42529
9,0.69253,0.3539
10,0.69255,0.35388
11,0.76346,0.42461
12,0.87481,0.43097
13,0.95249,0.51322
14,0.95309,0.51388
15,0.95163,0.51434
16,0.76627,0.57176
17,0.76526,0.57266
18,0.76548,0.63521
19,0.8744,0.61124
20,0.87497,0.61177
21,0.8744,0.73545
22,0.76681,0.73599
23,0.87506,0.73662
24,0.99901,0.83257
25,0.99998,0.83333
26,0.9984,0.83421
27,0.79844,0.94351
28,0.79685,0.94437
29,0.79658,0.94363
30,0.76506,0.84912
31,0.68674,0.91484
32,0.6435,0.86454
33,0.64299,0.86394
34,0.6435,0.86454
35,0.68674,0.91484
36,0.76474,0.84808
37,0.76557,0.84734
38,0.76558,0.84615
39,0.76564,0.73605
40,0.76439,0.63667
41,0.69027,0.6388
42,0.68952,0.63881
43,0.68874,0.63796
44,0.60905,0.52846
45,0.60821,0.52817
46,0.54765,0.58213
47,0.54662,0.58232
48,0.42201,0.55537
49,0.36001,0.48649
50,0.36,0.4865
51,0.4211,0.55568
52,0.37645,0.63848
53,0.37664,0.63901
54,0.45156,0.65243
55,0.45204,0.65318
56,0.45368,0.77711
57,0.52855,0.77886
58,0.5293,0.7796
59,0.5305,0.86096
60,0.39137,0.99926
61,0.39137,0.99926
62,0.53049,0.86096
63,0.52856,0.77959
64,0.45317,0.77779
65,0.37255,0.77682
66,0.37122,0.7767
67,0.37031,0.77653
68,0.31851,0.7652
69,0.31775,0.76503
70,0.31675,0.76425
71,0.25007,0.67903
72,0.23456,0.59185
73,0.23442,0.59
74,0.23442,0.59
75,0.23454,0.59184
76,0.24867,0.67858
77,0.14058,0.722
78,0.049,0.7231
79,0.17064,0.83318
80,0.06336,0.916
81,0.06251,0.91666
82,0.06336,0.916
83,0.17064,0.83318
84,0.04823,0.7231
85,0.04798,0.72223
86,0.14058,0.722
87,0.24867,0.67858
88,0.23454,0.59179
89,0.23329,0.58756
90,0.0942,0.47372
91,0.01629,0.54109
92,0.01567,0.54162
93,0.01628,0.54108
94,0.0931,0.4728
95,0.0948,0.47202
96,0.23186,0.44274
97,0.07905,0.33404
98,0.07714,0.33281
99,9.9E-4,0.2922
100,3.6E-4,0.29186
101,0.001,0.29219
102,0.07772,0.33179
103,0.15226,0.20558
104,0.10888,0.1778
105,0.10742,0.17759
106,0.10855,0.17703
107,0.13572,0.14777
108,0.14059,0.01495
109,0.14062,0.01391
110,0.14059,0.01495
111,0.13609,0.14731
112,0.13208,0.15798
113,0.15376,0.20503
114,0.25042,0.25049
115,0.31204,0.34438
116,0.2196,0.36183
117,0.23407,0.44278
118,0.23407,0.44278
119,0.2196,0.36183
120,0.31246,0.34455
121,0.3275,0.27874
122,0.25151,0.25056
123,0.32801,0.27773
124,0.37346,0.22408
125,0.37348,0.22317
126,0.31299,0.12579
127,0.31252,0.12502
128,0.31362,0.12534
129,0.45245,0.16589
130,0.46862,0.02887
131,0.46875,0.0278
132,0.46863,0.02888
133,0.45363,0.16641
134,0.53075,0.222
135,0.53143,0.22277
136,0.57721,0.30494
137,0.46941,0.30558
138,0.46859,0.30618
139,0.4231,0.41602
140,0.42327,0.41654
141,0.50004,0.40551
142,0.50076,0.40597
143,0.5314,0.47561
144,0.57841,0.30656
145,0.69194,0.35166
146,0.71655,0.30921
147,0.71646,0.30829
148,0.65709,0.23645
149,0.65662,0.23583
150,0.65687,0.23512
151,0.68734,0.15332
152,0.68701,0.15271
153,0.59469,0.12529
154,0.59394,0.12507
155,0.59518,0.12475
156,0.74871,0.08323
157,0.68804,7.3E-4
158,0.68756,8.0E-5
159,0.68805,7.3E-4
160,0.74998,0.08261
161,0.82833,0.01418
162,0.82925,0.01376
163,0.90858,0.00739
164,0.90954,0.00783
165,0.91024,0.00891
166,0.9354,0.05217
167,0.93573,0.05329
168,0.93722,0.13725
169,0.93722,0.13799
170,0.93647,0.13813
171,0.84433,0.15271
172,0.8434,0.15316
173,0.76597,0.2219
174,0.76614,0.22259
175,0.87736,0.28023
176,0.87882,0.28118
177,0.87935,0.28164
178,0.91391,0.31237
179,0.91503,0.31337
180,0.91508,0.31343
181,0.91529,0.31508
182,0.87456,0.43009
183,0.75425,0.45768
184,0.70023,0.50846
185,0.609,0.43154
186,0.53187,0.47916
187,0.53145,0.47991
188,0.53187,0.47916
189,0.60899,0.43155
,,,Distance,11.32157

De-Normalizing route

DONE!


 Success in writing TSP and LOG Files
 Program terminates at 2015-04-23 00:11:39
 Program completed in 2061747.0sec


------------------
 T : 2061.0 s
 I : 10001
 D : 767.0
------------------
