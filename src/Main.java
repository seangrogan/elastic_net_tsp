import java.awt.*;

public class Main {
    public static void main(String[] args) {
        int totalIter = 10000;
        int dispIter = 100;

        // Setting the base parameters ---------------------------------------------------------------------------------
        ParFile base = new ParFile();
        base.setAll("./src/data/tsp22.txt", "./src/log/tsp22_log.txt", "./src/out/tsp22_out.csv",
                2.0, 0.2, 0.20, 0.02, 0.99, 25, 2.5, 0.5, totalIter, dispIter);/*
        // Running the base parameters ---------------------------------------------------------------------------------
        runAll(base, elasticNetTSP, "_BASE");
        Toolkit.getDefaultToolkit().beep();
        // Running changes to alpha ------------------------------------------------------------------------------------
        base.setAll("./src/data/tsp22.txt", "./src/log/tsp22_log.txt", "./src/out/tsp22_out.csv",
                1.0, 0.2, 0.20, 0.02, 0.99, 25, 2.5, 0.5, totalIter, dispIter);
        runAll(base, elasticNetTSP, "_ALPHA1.0");
        Toolkit.getDefaultToolkit().beep();
        base.setAll("./src/data/tsp22.txt", "./src/log/tsp22_log.txt", "./src/out/tsp22_out.csv",
                3.0, 0.2, 0.20, 0.02, 0.99, 25, 2.5, 0.5, totalIter, dispIter);
        runAll(base, elasticNetTSP, "_ALPHA3.0");
        Toolkit.getDefaultToolkit().beep();
        // Running changes to beta -------------------------------------------------------------------------------------
        base.setAll("./src/data/tsp22.txt", "./src/log/tsp22_log.txt", "./src/out/tsp22_out.csv",
                2.0, 0.1, 0.20, 0.02, 0.99, 25, 2.5, 0.5, totalIter, dispIter);
        runAll(base, elasticNetTSP, "_BETA0.1");
        Toolkit.getDefaultToolkit().beep();
        base.setAll("./src/data/tsp22.txt", "./src/log/tsp22_log.txt", "./src/out/tsp22_out.csv",
                2.0, 0.3, 0.20, 0.02, 0.99, 25, 2.5, 0.5, totalIter, dispIter);
        runAll(base, elasticNetTSP, "_BETA0.3");
        Toolkit.getDefaultToolkit().beep();
        // Running changes to initial K --------------------------------------------------------------------------------
        base.setAll("./src/data/tsp22.txt", "./src/log/tsp22_log.txt", "./src/out/tsp22_out.csv",
                2.0, 0.2, 0.10, 0.02, 0.99, 25, 2.5, 0.5, totalIter, dispIter);
        runAll(base, "_initK0.1");
        Toolkit.getDefaultToolkit().beep();
        base.setAll("./src/data/tsp22.txt", "./src/log/tsp22_log.txt", "./src/out/tsp22_out.csv",
                2.0, 0.2, 0.30, 0.02, 0.99, 25, 2.5, 0.5, totalIter, dispIter);
        runAll(base, "_initk0.3");
        Toolkit.getDefaultToolkit().beep();
        // Running Changes to Epsilon ----------------------------------------------------------------------------------
        base.setAll("./src/data/tsp22.txt", "./src/log/tsp22_log.txt", "./src/out/tsp22_out.csv",
                2.0, 0.2, 0.20, 0.01, 0.99, 25, 2.5, 0.5, totalIter, dispIter);
        runAll(base, "_EPISLON0.01");
        Toolkit.getDefaultToolkit().beep();
        base.setAll("./src/data/tsp22.txt", "./src/log/tsp22_log.txt", "./src/out/tsp22_out.csv",
                2.0, 0.2, 0.20, 0.03, 0.99, 25, 2.5, 0.5, totalIter, dispIter);
        runAll(base, "_EPSILON0.03");
        Toolkit.getDefaultToolkit().beep();
        // Running changes to k-alpha ----------------------------------------------------------------------------------
        base.setAll("./src/data/tsp22.txt", "./src/log/tsp22_log.txt", "./src/out/tsp22_out.csv",
                2.0, 0.2, 0.20, 0.02, 0.80, 25, 2.5, 0.5, totalIter, dispIter);
        runAll(base, "_K-ALPHA0.80");
        Toolkit.getDefaultToolkit().beep();
        base.setAll("./src/data/tsp22.txt", "./src/log/tsp22_log.txt", "./src/out/tsp22_out.csv",
                2.0, 0.2, 0.20, 0.02, 0.70, 25, 2.5, 0.5, totalIter, dispIter);
        runAll(base, "_K-ALPHA0.70");
        Toolkit.getDefaultToolkit().beep();
        // Running changes to k-updating -------------------------------------------------------------------------------
        base.setAll("./src/data/tsp22.txt", "./src/log/tsp22_log.txt", "./src/out/tsp22_out.csv",
                2.0, 0.2, 0.20, 0.02, 0.99, 50, 2.5, 0.5, totalIter, dispIter);
        runAll(base, "_K-UPDATE50");
        Toolkit.getDefaultToolkit().beep();
        base.setAll("./src/data/tsp22.txt", "./src/log/tsp22_log.txt", "./src/out/tsp22_out.csv",
                2.0, 0.2, 0.20, 0.02, 0.99, 100, 2.5, 0.5, totalIter, dispIter);
        runAll(base, "_K-UPDATE100");
        Toolkit.getDefaultToolkit().beep();
        // running changes to the number of nodes ----------------------------------------------------------------------
       base.setAll("./src/data/tsp22.txt", "./src/log/tsp22_log.txt", "./src/out/tsp22_out.csv",
                2.0, 0.2, 0.20, 0.02, 0.99, 25, 1.0, 0.5, totalIter, dispIter);
        runAll(base, "_NEURALCOUNT1.0");
        Toolkit.getDefaultToolkit().beep();
        base.setAll("./src/data/tsp22.txt", "./src/log/tsp22_log.txt", "./src/out/tsp22_out.csv",
                2.0, 0.2, 0.20, 0.02, 0.99, 25, 2.0, 0.5, totalIter, dispIter);
        runAll(base, "_NEURALCOUNT2.0");
        Toolkit.getDefaultToolkit().beep();
        base.setAll("./src/data/tsp22.txt", "./src/log/tsp22_log.txt", "./src/out/tsp22_out.csv",
                2.0, 0.2, 0.20, 0.02, 0.99, 25, 3.0, 0.5, totalIter, dispIter);
        runAll(base, "_NEURALCOUNT3.0");
        Toolkit.getDefaultToolkit().beep();
        base.setAll("./src/data/tsp22.txt", "./src/log/tsp22_log.txt", "./src/out/tsp22_out.csv",
                2.0, 0.2, 0.20, 0.02, 0.99, 25, 5.0, 0.5, totalIter, dispIter);
        runAll(base, "_NEURALCOUNT5.0");
        Toolkit.getDefaultToolkit().beep();
        // Running changes to the initial radius size ------------------------------------------------------------------
        base.setAll("./src/data/tsp22.txt", "./src/log/tsp22_log.txt", "./src/out/tsp22_out.csv",
                2.0, 0.2, 0.20, 0.02, 0.99, 25, 2.5, 0.25, totalIter, dispIter);
        runAll(base, "_RADIUS0.25");
        Toolkit.getDefaultToolkit().beep();*/
        base.setAll("./src/data/tsp22.txt", "./src/log/tsp22_log.txt", "./src/out/tsp22_out.csv",
                2.0, 0.2, 0.20, 0.02, 0.99, 25, 2.5, 0.75, totalIter, dispIter);
        runAll(base, "_RADIUS0.75");
        Toolkit.getDefaultToolkit().beep();

        for (int i = 0; i < 60*10; i++) {
            try {
                Thread.sleep(1000);
                Toolkit.getDefaultToolkit().beep();
            }
            catch (Exception e ){
                System.out.println("Beep Exception");
            }
        }
    }

    public static void runAll(ParFile base, String fileAppend){
        ParFile tsp22_base = base.copy();
        tsp22_base.setFileNames("./src/data/tsp22.txt",
                "./src/log/tsp22"+fileAppend+"_log.txt",
                "./src/out/tsp22"+fileAppend+"_out.csv");
        ElasticNetTSP.main(tsp22_base.arguments());

        Toolkit.getDefaultToolkit().beep();

        ParFile tsp30_base = base.copy();
        tsp30_base.setFileNames("./src/data/tsp30.txt",
                "./src/log/tsp30"+fileAppend+"_log.txt",
                "./src/out/tsp30"+fileAppend+"_out.csv");
        ElasticNetTSP.main(tsp30_base.arguments());

        Toolkit.getDefaultToolkit().beep();

        ParFile tsp51_base = base.copy();
        tsp51_base.setFileNames("./src/data/tsp51.txt",
                "./src/log/tsp51"+fileAppend+"_log.txt",
                "./src/out/tsp51"+fileAppend+"_out.csv");
        ElasticNetTSP.main(tsp51_base.arguments());

        Toolkit.getDefaultToolkit().beep();

        ParFile tsp76_base = base.copy();
        tsp76_base.setFileNames("./src/data/tsp76.txt",
                "./src/log/tsp76"+fileAppend+"_log.txt",
                "./src/out/tsp76"+fileAppend+"_out.csv");
        ElasticNetTSP.main(tsp76_base.arguments());

        Toolkit.getDefaultToolkit().beep();

        ParFile tsp100_base = base.copy();
        tsp100_base.setFileNames("./src/data/tsp100.txt",
                "./src/log/tsp100"+fileAppend+"_log.txt",
                "./src/out/tsp100"+fileAppend+"_out.csv");
        ElasticNetTSP.main(tsp100_base.arguments());

        Toolkit.getDefaultToolkit().beep();

        /*
        ParFile tsp442_base = base.copy();
        tsp442_base.setFileNames("./src/data/tsp442.txt",
                "./src/log/tsp442"+fileAppend+"_log.txt",
                "./src/out/tsp442"+fileAppend+"_out.csv");
        elasticNetTSP.main(tsp442_base.arguments());

        Toolkit.getDefaultToolkit().beep();*/
    }
}
