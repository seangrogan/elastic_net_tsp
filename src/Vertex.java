public class Vertex {
    private double x;
    private double y;
    private int key;
    private double[] location;

    public Vertex(int key, double x, double y){
        this.key = key;
        this.x = x;
        this.y = y;
        double[] l = {x, y};
        this.location = l;
    }
    public Vertex(int key, double[] location){
        this.key = key;
        this.x = location[0];
        this.y = location[1];
        this.location = location;
    }
    public double calculateEuclideanDistance(Vertex other){
        double delX = this.x() - other.x();
        double delY = this.y() - other.y();
        double dist = Math.sqrt(Math.pow(delX, 2) + Math.pow(delY, 2));
        return dist;
    }
    public double x(){
        return this.x;
    }
    public double y(){
        return this.y;
    }
    public int getKey(){
        return this.key;
    }
    public double[] getLocation(){
        return this.location;
    }
    public void move(double del_x, double del_y){
        this.x += del_x;
        this.y += del_y;
    }
}
