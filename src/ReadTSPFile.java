import java.io.File;
import java.util.Scanner;

public class ReadTSPFile {
    private String path;
    private Scanner scanner;

    public void open(String fileName){
        this.path = fileName;
        try {
            scanner = new Scanner(new File(fileName));
        }
        catch (Exception e) {
            System.out.println("Could not open file : " + fileName);
        }
    }

    public void read(ElasticNet en){
        try {
            while (scanner.hasNext()) {
                int k = Integer.parseInt(scanner.next());
                int x = Integer.parseInt(scanner.next());
                int y = Integer.parseInt(scanner.next());
                Vertex v = new Vertex(k, x, y);
                en.addCity(v);
            }
        }
        catch (Exception e){
            System.out.println("Could not read file : " + this.path);
        }
    }

    public void close() {
        this.scanner.close();
    }
}
