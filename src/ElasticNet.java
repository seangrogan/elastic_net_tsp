import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Vector;

public class ElasticNet {
    private double[] center;
    private Vector<Vertex> cities;
    private Vector<Vertex> net;

    private double sumX;
    private double sumY;
    private double K;
    private double distance;
    private int numIter;
    private ParFile par;
    private int netSize;
    private Vector<Vector<Double>> weights;
    private Vector<Double> delX;
    private Vector<Double> delY;
    private Vector<Vertex> citiesRegular;
    public double minX;
    public double minY;
    public double maxX;
    public double maxY;

    /**
     * @param par A par ADT which will be passed into the elastic net program
    */
    public ElasticNet(ParFile par){
        this.numIter = 0;
        this.center = new double[2];
        this.cities = new Vector<Vertex>();
        this.citiesRegular = new Vector<Vertex>();
        this.net = new Vector<Vertex>();
        this.netSize = 0;

        this.K = par.initK;
        this.par = par;

        this.minX = Double.MAX_VALUE;
        this.minY = Double.MAX_VALUE;
        this.maxX = Double.MIN_VALUE;
        this.maxY = Double.MIN_VALUE;
        this.delX = new Vector<Double>();
        this.delY = new Vector<Double>();
    }
    /**
     * Adds a city to the elastic map
     * @param v a fixed vertex
     */
    public void addCity(Vertex v) {
        this.citiesRegular.add(v);

        this.minX = Math.min(v.x(), this.minX);
        this.minY = Math.min(v.y(), this.minY);
        this.maxX = Math.max(v.x(), this.maxX);
        this.maxY = Math.max(v.y(), this.maxY);
    }
    /**
     * Sets the cities to be located between 0-1
     */
    public void normalizeCities(){
        this.cities = new Vector<Vertex>(this.citiesRegular.size(), 1);
        for (Vertex vOld : this.citiesRegular) {
            double x = (vOld.x() - minX) / (maxX - minX);
            double y = (vOld.y() - minY) / (maxY - minY);
            Vertex v = new Vertex(vOld.getKey(), x, y);
            this.sumX += v.x();
            this.sumY += v.y();
            cities.add(v);
        }
        this.average();
    }
    /**
     * Creates a net with a center at the average of the cities
     */
    public void buildNet(){
        this.average();
        this.net.clear();
        this.netSize = (int) (this.par.numNeuronsFactor * this.cities.size());
        Vector<Double> theta = evenSpace(0, 2*Math.PI, this.netSize);
        for (int i = 0; i < this.netSize; i++) {
            double x = Math.cos(theta.get(i));
            double y = Math.sin(theta.get(i));
            x *= par.radius;
            y *= par.radius;
            x += this.center[0];
            y += this.center[1];

            double[] c = {x,y};

            this.net.add(new Vertex(i, c));
        }
    }
    /**
     * Builds the center of the net
     */
    private void average(){
        this.center[0] = this.sumX / this.cities.size();
        this.center[1] = this.sumY / this.cities.size();
    }
    /**
     * Iterates one iteration of the elastic net
     */
    public void iteration(){
        this.numIter ++;
        this.updateK();
        this.updateWeights();
        this.updateNeurons();
    }
    /**
     * Yields the appropriate stopping criteria if it complete
     * @return true if complete, false otherwise
     */
    public boolean stopCriteria(){
        return ((this.worstDistance() < this.par.epsilon) || (this.numIter > this.par.maxNumberIter));
    }
    public double worstDistance(){
        double worst = Double.MIN_VALUE;
        for (Vertex city : this.cities) {
            double best = Double.MAX_VALUE;
            for (Vertex aNet : this.net) {
                double d = city.calculateEuclideanDistance(aNet);
                best = Math.min(best, d);
            }
            worst = Math.max(best, worst);
        }
        return worst;
    }
    public double getDistance(){
        this.distance = 0;
        int M = this.net.size();
        for (int j = 0; j < M; j++) {
            this.distance += this.net.get(j).calculateEuclideanDistance(this.net.get(myModulus(j + 1, M)));

        }
        return this.distance;
    }
    public String tourCSV(){
        String out = "";
        for (Vertex aNet : this.net) {
            out += aNet.getKey() + "," + round(aNet.x(), 5) + "," + round(aNet.y(), 5) + "\n";
        }
        out += ",,,Distance," + round(this.distance, 5);
        return out;
    }
    public String nomalizedCustomersCSV(){
        String out = "";
        for (Vertex city : this.cities) {
            out += city.getKey() + "," + round(city.x(), 5) + "," + round(city.y(), 5) + "\n";
        }
        out += ",,,Distance," + round(this.distance, 5);
        return out;
    }
    private void updateK(){
        if((this.numIter % this.par.KUpdatePeriod) == 0)
            this.K = Math.max(0.01, this.par.KAlpha * this.K);
    }
    private void updateWeights(){
        int N = this.cities.size();
        int M = this.net.size();
        this.weights = new Vector<Vector<Double>>(N);
        for (int i = 0; i < N; i++) {
            this.weights.add(new Vector<Double>(M));
            for (int j = 0; j < M; j++) {
                double w = w_ij(i, j);
                this.weights.get(i).add(w);
            }
        }
        calcDeltasX();
        calcDeltasY();
    }
    private void updateNeurons(){
        for (int j = 0; j < this.net.size(); j++) {
            this.net.get(j).move(deltaX(j), deltaY(j));
        }
    }
    private double deltaX(int j){
        double out = 0;
        for (int i = 0; i < this.cities.size(); i++) {
            double delta = (this.cities.get(i).x() - this.net.get(j).x());
            out += (this.weights.get(i).get(j) * delta);
        }

        double netDelta = delX.get(j);
        double in = this.par.beta * this.K * netDelta;
        return (this.par.alpha * out) + in;
    }
    private double deltaY(int j){
        double out = 0;
        for (int i = 0; i < this.cities.size(); i++) {
            out += (this.weights.get(i).get(j) * (this.cities.get(i).y() - this.net.get(j).y()));
        }
        double netDelta = delY.get(j);
        double in = this.par.beta * this.K * netDelta;
        return (this.par.alpha * out) + in;
    }
    private double w_ij(int i, int j){
        double d_Xi_Yj =  this.cities.get(i).calculateEuclideanDistance(this.net.get(j));
        double numerator = this.phi(d_Xi_Yj, this.K);
        double denomerator = 0;
        for (Vertex aNet : this.net) {
            denomerator += phi(this.cities.get(i).calculateEuclideanDistance(aNet), this.K);
        }
        if (denomerator == 0) denomerator += 1;
        return numerator/denomerator;
    }
    private void calcDeltasX(){
        this.delX.clear();
        int s = this.net.size();
        for (int j = 0; j < this.net.size(); j++) {
            this.delX.add((this.net.get(myModulus(j + 1, s)).x() + this.net.get(myModulus(j - 1, s)).x() - 2 * this.net.get(j).x()));
        }
    }
    private void calcDeltasY(){
        this.delY.clear();
        int s = this.net.size();
        for (int j = 0; j < this.net.size(); j++) {
            this.delY.add((this.net.get(myModulus(j + 1, s)).y() + this.net.get(myModulus(j - 1, s)).y() - 2 * this.net.get(j).y()));
        }
    }
    private double phi(double d, double K){
        double n1 = -1 * Math.pow(d, 2);
        double n2 = 2 * Math.pow(K, 2);
        double e = n1/n2;
        return Math.exp(e);
    }
    private int myModulus(int n, int m){
        int mod = n % m;
        if (mod < 0) {
            mod += m;
        }
        return mod;
    }
    private double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
    private Vector<Double> evenSpace(double begin, double end, int count){
        Vector<Double> space = new Vector<Double>(count, 1);
        double inc = Math.abs(end - begin)/count;
        double v = begin;
        while(v < end){
            space.add(v);
            v += inc;
        }
        return space;
    }
    public Vector<Vertex> getNet(){
        return this.net;
    }
    public Vector<Vertex> getCities(){
        return this.citiesRegular;
    }
    public Vector<Vertex> getNormalizedCities(){
        return this.cities;
    }
}
