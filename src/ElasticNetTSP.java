import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;


public class ElasticNetTSP {
    public static void main(String[] args) {
        long timeStart = System.currentTimeMillis();

        // reading parameters from the arguments
        ParFile par = new ParFile();
        parseArgs(args, par);

        System.out.println(stringInitialization());
        writeLOG(par, stringInitialization());

        // Creating the elastic net
        ElasticNet elasticNet = new ElasticNet(par);
        parseFile(par, elasticNet);
        printAlgo(par);

        System.out.println(stringParFile(par));
        writeLOG(par, stringParFile(par));

        // building the elastic net
        int iter = 0;
        elasticNet.normalizeCities();
        elasticNet.buildNet();
        writeNormalizedCustomers(par, elasticNet);

        // running the elastic net
        long AlgoStart = System.currentTimeMillis();
        while(!elasticNet.stopCriteria()){
            elasticNet.iteration();
            if(iter%par.dispIter == 0){
                System.out.println(stringStatus(iter, elasticNet.getDistance(), elasticNet.worstDistance(), timeStart));
                writeLOG(par, stringStatus(iter, elasticNet.getDistance(), elasticNet.worstDistance(), timeStart));
                writeTSP(par, elasticNet);
            }
            iter++;
        }

        System.out.println(stringStatus(iter, elasticNet.getDistance(), elasticNet.worstDistance(), timeStart));
        writeLOG(par, stringStatus(iter, elasticNet.getDistance(), elasticNet.worstDistance(), timeStart));

        String s = competionMessage(par, elasticNet, AlgoStart);
        double t = (System.currentTimeMillis() - AlgoStart)/1000;
        writeLOG(par, s);
        System.out.println(s);

        writeTSP(par, elasticNet);
        writeLOG(par, elasticNet.tourCSV());

        writeLOG(par, "\nDe-Normalizing route\n");
        System.out.println("De-Normalizing route");

        Tour tour = new Tour(elasticNet, par);
        tour.developTour();
        tour.distance();
        writeFinalTSP(par, tour);
        writeLOG(par, "DONE!\n\n");
        System.out.println("DONE!\n");


        System.out.println(writingMessage(timeStart));
        writeLOG(par, writingMessage(timeStart));

        String sss = "\n------------------\n";
        sss += " T : " + t + " s\n";
        sss += " I : " + iter + "\n";
        sss += " D : " + round(tour.distance(),0) + "\n";
        sss += "------------------";
        writeLOG(par, sss);
        System.out.println(sss);
    }

    private static void parseArgs(String[] args, ParFile par){
        try{
            par.inFile = args[0].substring(1);
            par.logFile = args[1].substring(1);
            par.outFile = args[2].substring(1);

            par.alpha = Double.parseDouble(args[3].substring(1));
            par.beta = Double.parseDouble(args[4].substring(1));
            par.initK = Double.parseDouble(args[5].substring(1));
            par.epsilon = Double.parseDouble(args[6].substring(1));

            par.KAlpha = Double.parseDouble(args[7].substring(1));
            par.KUpdatePeriod = (int) Double.parseDouble(args[8].substring(1));
            par.numNeuronsFactor = Double.parseDouble(args[9].substring(1));
            par.radius = Double.parseDouble(args[10].substring(1));

            par.maxNumberIter = Integer.parseInt(args[11].substring(1));
            par.dispIter = Integer.parseInt(args[12].substring(1));
        }
        catch (Exception e){
            System.out.println("Error reading arguments:");
            System.out.println("     0 : -fileName");
            System.out.println("     1 : -outputFile");
            System.out.println("     2 : -logFile");
            System.out.println("     3 : -alpha");
            System.out.println("     4 : -beta");
            System.out.println("     5 : -initK");
            System.out.println("     6 : -epsilon");
            System.out.println("     7 : -KAlpha");
            System.out.println("     8 : -KUpdatePeriod");
            System.out.println("     9 : -numNeuronsFactor");
            System.out.println("     10: -radius");
            System.out.println("     11: -maxNumberIter");
            System.out.println("     12: -displyIter/saveIter");

        }
    }
    private static void parseFile(ParFile par, ElasticNet en){
        ReadTSPFile readFile = new ReadTSPFile();
        System.out.println(" Reading File " + par.inFile +"...");
        readFile.open(par.inFile);
        readFile.read(en);
        readFile.close();
        System.out.println(" Successfully read " + par.inFile +"!");
    }
    private static void writeTSP(ParFile par, ElasticNet en){
        OutFile TSPOutput = new OutFile(par.outFile, false);
        TSPOutput.writeLine(en.tourCSV());
        TSPOutput.closeFile();
    }
    private static void writeNormalizedCustomers(ParFile par, ElasticNet en){
        String normCust = par.inFile;

        normCust = normCust.substring(0, normCust.length()-4);
        normCust += "_NORM.csv";

        OutFile normCustOut = new OutFile(normCust, false);
        normCustOut.writeLine(en.nomalizedCustomersCSV());
        normCustOut.closeFile();
    }
    private static void writeLOG(ParFile par, String line){
        OutFile LOGOutput = new OutFile(par.logFile, true);
        LOGOutput.writeLine(line);
        LOGOutput.closeFile();
    }
    private static void writeFinalTSP(ParFile par, Tour tour){
        String out = par.outFile;

        out = out.substring(0, out.length()-4);
        out += "_FINAL.csv";

        OutFile TSPOutput = new OutFile(out, false);
        TSPOutput.writeLine(tour.tourCSV());
        TSPOutput.closeFile();
    }

    private static String stringInitialization(){
        String s = "";
        s += "+----------------------------+" + "\n";
        s += "| Sean Grogan's Elastic Net  |" + "\n";
        s += "+----------------------------+" + "\n";
        s += "Beginning: " + stringDate() + "\n";
        return s;

    }
    private static void printAlgo(ParFile p){
        System.out.println("+----------------------------+");
        System.out.println("| Beginning Algorithm Steps  |");
        System.out.println("+----------------------------+");
        System.out.println("Stopping Criteria: ");
        System.out.println("  Maximum Iterations : " + p.maxNumberIter);
        System.out.println("  Worst distance     : " + p.epsilon);
    }
    private static String stringStatus(int Iter, double dist, double worstDist, double timeStart){
        String s = "";
        s += "  " + Iter + ": " + "\n";
        s += "\t Current Distance : " + round(dist, 5) + "\n";
        s += "\t Worst Distance   : " + round(worstDist, 5) + "\n";
        s += "\t Current Time     : " + round((System.currentTimeMillis() - timeStart)/1000, 2) + " sec\n";
        return s;
    }
    private static String stringDate(){
        Date date = new Date() ;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss") ;
        return dateFormat.format(date);
    }
    private static String stringParFile(ParFile p){
        String s = "";
        s += "Paramters:" + "\n";
        s += p.log();
        return s;

    }
    private static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
    private static String competionMessage(ParFile p, ElasticNet e, double AlgoStart){
        String s = "";
        if(e.worstDistance() < p.epsilon){
            s += "Success!\n";
            s += "  Successfully completed an elastic net with\n";
            s += "  an accuracy of " + round(e.worstDistance(), 5) + " units\n";
            s += "\n";
            s += " Algorithm completed on " + stringDate() +"\n";
            s += " Algorithm completed in " + round(System.currentTimeMillis() - AlgoStart,2)/1000 + "sec\n";
        }
        else{
            s += "Incomplete!\n";
            s += "  Unsuccessfully completed an elastic net with\n";
            s += "  an accuracy of " + round(e.worstDistance(), 5) + "units\n";
            s += "  (Elastic net ended because of max iter count)\n";
            s += "\n";
            s += " Algorithm completed on " + stringDate() +"\n";
            s += " Algorithm completed in " + round(System.currentTimeMillis() - AlgoStart,2)/1000 + "sec\n";
        }
        return s;
    }
    private static String writingMessage(double timeStart){
        String s = "";

        s+= " Success in writing TSP and LOG Files\n";
        s+= " Program terminates at " + stringDate() +"\n";
        s+= " Program completed in " + round(System.currentTimeMillis() - timeStart,2) + "sec\n";

        return s;
    }

}
