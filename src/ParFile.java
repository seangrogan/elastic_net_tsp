public class ParFile {
    public String inFile;
    public String outFile;
    public String logFile;
    public double alpha;
    public double beta;
    public double initK;
    public double epsilon;
    public double KAlpha;
    public double KUpdatePeriod;
    public double numNeuronsFactor;
    public double radius;
    public int maxNumberIter;
    public int dispIter;

    public ParFile copy(){
        ParFile p = new ParFile();
        p.setAll(inFile, outFile, logFile, alpha, beta, initK, epsilon, KAlpha, KUpdatePeriod,
                numNeuronsFactor, radius, maxNumberIter, dispIter);
        return p;
    }
    public void setAll(String inFile, String logFile, String outFile, double alpha, double beta, double initK,
                       double epsilon, double KAlpha, double KUpdatePeriod, double numNeuronsFactor, double radius,
                       int maxNumberIter, int dispIter){
        this.inFile = inFile;
        this.outFile = outFile;
        this.logFile = logFile;

        this.alpha = alpha;
        this.beta = beta;
        this.initK = initK;
        this.epsilon = epsilon;
        this.KAlpha = KAlpha;
        this.KUpdatePeriod = KUpdatePeriod;
        this.numNeuronsFactor = numNeuronsFactor;
        this.radius = radius;

        this.maxNumberIter = maxNumberIter;
        this.dispIter = dispIter;
    }
    public void setFileNames(String inFile, String logFile, String outFile){
        this.inFile = inFile;
        this.outFile = outFile;
        this.logFile = logFile;
    }
    public String log(){
        String s = "";

        s += "inFile     " + inFile + "\n";
        s += "logFile    " + logFile + "\n";
        s += "outFile    " + outFile + "\n";

        s += "alpha      " + alpha + "\n";
        s += "beta       " + beta + "\n";
        s += "initK      " + initK + "\n";
        s += "epsilon    " + epsilon + "\n";

        s += "KAlpha     " + KAlpha + "\n";
        s += "KUpdate    " + KUpdatePeriod + "\n";
        s += "NeuronsFac " + numNeuronsFactor + "\n";
        s += "radius     " + radius + "\n";
        s += "MaxIter    " + maxNumberIter + "\n";

        s += "DispIter   " + dispIter + "\n";

        return s;
    }
    public String[] arguments(){
        String[] s = {
                "-"+inFile,
                "-"+logFile,
                "-"+outFile,
                "-"+alpha,
                "-"+beta,
                "-"+initK,
                "-"+epsilon,
                "-"+KAlpha,
                "-"+KUpdatePeriod,
                "-"+numNeuronsFactor,
                "-"+radius,
                "-"+maxNumberIter,
                "-"+dispIter
        };
        return s;
    }
}
