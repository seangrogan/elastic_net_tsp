import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Vector;

public class Tour {
    private Vector<Vertex> normalizedTour;
    private Vector<Vertex> Tour;
    private Vector<Vertex> cities;
    private Vector<Vertex> normalizedCities;
    private ParFile par;
    private ElasticNet elasticNet;
    private double distiance;

    public Tour(ElasticNet elasticNet, ParFile par){
        this.elasticNet = elasticNet;
        this.par = par;
        this.normalizedTour = (Vector) elasticNet.getNet().clone();
        this.cities = (Vector) elasticNet.getCities().clone();
        this.Tour = new Vector<Vertex>(this.cities.size());
        this.normalizedCities = (Vector) elasticNet.getNormalizedCities().clone();
    }
    public void developTour(){
        if(elasticNet.worstDistance() < par.epsilon) {
            for (int j = 0; j < this.normalizedTour.size(); j++) {
                for (int i = 0; i < this.normalizedCities.size(); i++) {
                    if (this.normalizedTour.get(j).calculateEuclideanDistance(this.normalizedCities.get(i)) < par.epsilon) {
                        Vertex v = this.normalizedCities.remove(i);
                        int k = v.getKey();
                        for (int l = 0; l < this.cities.size(); l++) {
                            if (k == this.cities.get(l).getKey()) {
                                v = this.cities.remove(l);
                                this.Tour.add(v);
                            }
                        }
                    }
                }
            }
        }
        else {
            for (int j = 0; j < this.normalizedTour.size(); j++) {
                Vertex v = this.normalizedTour.get(j);
                double x = v.x() * (this.elasticNet.maxX - this.elasticNet.minX) + this.elasticNet.minX;
                double y = v.y() * (this.elasticNet.maxY - this.elasticNet.minY) + this.elasticNet.minY;
                this.Tour.add(new Vertex(v.getKey(), x, y));
            }
        }
    }
    public double distance(){
        this.distiance = 0;
        int M = this.Tour.size();
        for (int j = 0; j < M; j++) {
            this.distiance += this.Tour.get(j).calculateEuclideanDistance(this.Tour.get(myModulus(j+1, M)));

        }
        return this.distiance;
    }
    public String tourCSV(){
        String out = "";
        for (int i = 0; i < this.Tour.size() ; i++) {
            out += this.Tour.get(i).getKey()+","+ round(this.Tour.get(i).x(), 0) +"," + round(this.Tour.get(i).y(), 0)+"\n";
        }
        out += ",,,Distance," + round(this.distiance, 5);
        return out;
    }
    private double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
    private int myModulus(int n, int m){
        int mod = n % m;
        if (mod < 0) {
            mod += m;
        }
        return mod;
    }
}
